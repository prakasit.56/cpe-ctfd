import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as UserTokensModel from 'src/models/user-tokens'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'
import { AuthService } from 'src/authentication/auth.service'

@Injectable()
export class UserTokensService {
  constructor(
    private prisma: PrismaService,
    private authService: AuthService,
  ) {}

  async create(data: any): Promise<UserTokensModel.UserTokens | Error> {
    /**
     * Create User record
     */
    try {
      // Insert User record to database
      return await this.prisma.userTokens.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: UserTokensModel.FindManyUserTokensArgs,
    select: any,
  ): Promise<UserTokensModel.UserTokens[] | Error> {
    /**
     * Get User records
     */
    try {
      // Get User records from database
      const result = await this.prisma.userTokens.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: UserTokensModel.UpdateOneUserTokensArgs,
    select: any,
  ): Promise<UserTokensModel.UserTokens | Error> {
    /**
     * Update or create UserTokens record
     */
    try {
      // Update User record
      return await this.prisma.userTokens.update({ ...args, ...select })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: UserTokensModel.DeleteOneUserTokensArgs,
  ): Promise<UserTokensModel.UserTokens | Error> {
    /**
     * Delete UserTokens record
     */
    try {
      // Delete User record
      return await this.prisma.userTokens.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async deleteMany(
    args: UserTokensModel.DeleteManyUserTokensArgs,
  ): Promise<number | Error> {
    /**
     * Delete UserTokens record
     */
    try {
      // Delete User record
      return (await this.prisma.userTokens.deleteMany({ ...args })).count
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
