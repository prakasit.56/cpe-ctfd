import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as TutorialContentsModel from 'src/models/tutorial-contents'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class TutorialContentsService {
  constructor(private prisma: PrismaService) {}

  async create(
    data: any,
  ): Promise<TutorialContentsModel.TutorialContents | Error> {
    /**
     * Create tutorial contents record
     */
    try {
      // Insert tutorial contents record to database
      return await this.prisma.tutorialContents.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: TutorialContentsModel.FindManyTutorialContentsArgs,
    select: any,
  ): Promise<TutorialContentsModel.TutorialContents[] | Error> {
    /**
     * Get tutorial contents records
     */
    try {
      // Get tutorial contents records from database
      const result = await this.prisma.tutorialContents.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: TutorialContentsModel.UpdateOneTutorialContentsArgs,
    select: any,
  ): Promise<TutorialContentsModel.TutorialContents | Error> {
    /**
     * Update or create tutorial contents record
     */
    try {
      // Update tutorial contents record
      return await this.prisma.tutorialContents.update({ ...args, ...select })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: TutorialContentsModel.DeleteOneTutorialContentsArgs,
  ): Promise<TutorialContentsModel.TutorialContents | Error> {
    /**
     * Delete tutorial contents record
     */
    try {
      // Delete tutorial contents record
      return await this.prisma.tutorialContents.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
