import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as CategoriesModel from 'src/models/categories'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class CategoriesService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<CategoriesModel.Categories | Error> {
    /**
     * Create categories record
     */
    try {
      // Insert categories record to database
      return await this.prisma.categories.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: CategoriesModel.FindManyCategoriesArgs,
    select: any,
  ): Promise<CategoriesModel.Categories[] | Error> {
    /**
     * Get categories records
     */
    try {
      // Get categories records from database
      const result = await this.prisma.categories.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: CategoriesModel.UpdateOneCategoriesArgs,
    select: any,
  ): Promise<CategoriesModel.Categories | Error> {
    /**
     * Update or create categories record
     */
    try {
      // Update categories record
      return await this.prisma.categories.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: CategoriesModel.DeleteOneCategoriesArgs,
  ): Promise<CategoriesModel.Categories | Error> {
    /**
     * Delete categories record
     */
    try {
      // Delete categories record
      return await this.prisma.categories.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
