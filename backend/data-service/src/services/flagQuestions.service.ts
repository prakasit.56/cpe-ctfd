import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as FlagQuestionsModel from 'src/models/flag-questions'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class FlagQuestionsService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<FlagQuestionsModel.FlagQuestions | Error> {
    /**
     * Create flag question record
     */
    try {
      // Insert flag question record to database
      return await this.prisma.flagQuestions.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: FlagQuestionsModel.FindManyFlagQuestionsArgs,
    select: any,
  ): Promise<FlagQuestionsModel.FlagQuestions[] | Error> {
    /**
     * Get flag question records
     */
    try {
      // Get flag question records from database
      const result = await this.prisma.flagQuestions.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: FlagQuestionsModel.UpdateOneFlagQuestionsArgs,
    select: any,
  ): Promise<FlagQuestionsModel.FlagQuestions | Error> {
    /**
     * Update or create flag question record
     */
    try {
      // Update flag question record
      return await this.prisma.flagQuestions.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: FlagQuestionsModel.DeleteOneFlagQuestionsArgs,
  ): Promise<FlagQuestionsModel.FlagQuestions | Error> {
    /**
     * Delete flag question record
     */
    try {
      // Delete flag question record
      return await this.prisma.flagQuestions.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
