import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as LevelsModel from 'src/models/levels'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class LevelsService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<LevelsModel.Levels | Error> {
    /**
     * Create level record
     */
    try {
      // Insert level record to database
      return await this.prisma.levels.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: LevelsModel.FindManyLevelsArgs,
    select: any,
  ): Promise<LevelsModel.Levels[] | Error> {
    /**
     * Get level records
     */
    try {
      // Get level records from database
      const result = await this.prisma.levels.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: LevelsModel.UpdateOneLevelsArgs,
    select: any,
  ): Promise<LevelsModel.Levels | Error> {
    /**
     * Update or create level record
     */
    try {
      // Update level record
      return await this.prisma.levels.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: LevelsModel.DeleteOneLevelsArgs,
  ): Promise<LevelsModel.Levels | Error> {
    /**
     * Delete level record
     */
    try {
      // Delete level record
      return await this.prisma.levels.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
