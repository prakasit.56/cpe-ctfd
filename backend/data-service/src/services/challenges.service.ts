import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as ChallengesModel from 'src/models/challenges'
import * as UsersModel from 'src/models/users'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'
import { NodeMailerService } from 'src/providers/node-mailer/node-mailer.service'

@Injectable()
export class ChallengesService {
  constructor(
    private prisma: PrismaService,
    private mailerService: NodeMailerService,
  ) {}

  async create(
    data: any,
    user: UsersModel.Users,
  ): Promise<ChallengesModel.Challenges | boolean | Error> {
    /**
     * Create challenges record
     */
    try {
      if (user.user_role.name !== UserRoleModel.Role.ADMIN) {
        const result = await this.mailerService.sendEmailToRequestExam(
          data.data,
          user.email,
        )
        if (result instanceof Error) return result

        return {
          challenge_id: '',
          question: '',
          name: '',
          description: '',
          level_id: '',
          types_id: '',
          categories_id: '',
          award_badge: '',
          scorce: 0,
          max_time: 0,
          max_cost: 0,
          createdAt: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
          updatedAt: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
        }
      }
      // Insert challenges record to database
      return await this.prisma.challenges.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: ChallengesModel.FindManyChallengesArgs,
    select: any,
  ): Promise<ChallengesModel.Challenges[] | Error> {
    /**
     * Get challenges records
     */
    try {
      // Get challenges records from database
      const result = await this.prisma.challenges.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: ChallengesModel.UpdateOneChallengesArgs,
    select: any,
  ): Promise<ChallengesModel.Challenges | Error> {
    /**
     * Update or create challenges record
     */
    try {
      // Update challenges record
      return await this.prisma.challenges.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: ChallengesModel.DeleteOneChallengesArgs,
  ): Promise<ChallengesModel.Challenges | Error> {
    /**
     * Delete challenges record
     */
    try {
      // Delete challenges record
      return await this.prisma.challenges.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async autoGrade(
    args: ChallengesModel.ChallengesAutoGrade,
    user: UsersModel.Users,
  ): Promise<boolean | Error> {
    /**
     * Auto grade challenges record
     */
    try {
      let result
      let score = 0
      if (args.multipleQuestions) {
        result = await this.prisma.challenges.findFirst({
          where: {
            AND: [{ challenge_id: args.challengeId }],
          },
          select: {
            scorce: true,
            max_time: true,
            max_cost: true,
            multipleQuestions: {
              select: {
                multiple_question_id: true,
                multipleChoiceQuestion: {
                  select: {
                    multiple_choice_question_id: true,
                    correct_flag: true,
                  },
                },
              },
            },
            badges: {
              select: {
                badge_id: true,
              },
            },
          },
        })

        args.multipleQuestions.forEach((mutipleQuestionArg) => {
          const correctChoice = result.multipleQuestions
            .filter(
              (mutipleQuestionRes) =>
                mutipleQuestionRes.multiple_question_id ===
                mutipleQuestionArg.multipleQuestionId,
            )[0]
            .multipleChoiceQuestion.filter(
              (multipleChoiceQuestionRes) =>
                multipleChoiceQuestionRes.correct_flag,
            )[0].multiple_choice_question_id
          score +=
            correctChoice === mutipleQuestionArg.multipleChoiceQuestionId &&
            !mutipleQuestionArg.hint
              ? 1
              : 0
        })

        score /= args.multipleQuestions.length
      }

      if (args.shortAnswerQuestionAutoGrade) {
        result = await this.prisma.challenges.findFirst({
          where: {
            challenge_id: args.challengeId,
          },
          select: {
            scorce: true,
            max_time: true,
            max_cost: true,
            badges: {
              select: {
                badge_id: true,
              },
            },
            shortAnswerQuestions: {
              select: {
                short_answer_question_id: true,
                answer: true,
              },
            },
          },
        })

        args.shortAnswerQuestionAutoGrade.forEach((shortAnswerQuestionArgs) => {
          const correctAnswer = result.shortAnswerQuestions.filter(
            (shortAnswerQuestionRes) =>
              shortAnswerQuestionRes.short_answer_question_id ===
              shortAnswerQuestionArgs.shortAnswerQuestionId,
          )[0].answer

          score +=
            correctAnswer === shortAnswerQuestionArgs.answer &&
            !shortAnswerQuestionArgs.hint
              ? 1
              : 0
        })
        score /= args.shortAnswerQuestionAutoGrade.length
      }

      if (args.flagQuestionAutoGrade) {
        result = await this.prisma.challenges.findFirst({
          where: {
            challenge_id: args.challengeId,
          },
          select: {
            scorce: true,
            max_time: true,
            max_cost: true,
            flagQuestions: {
              select: {
                flag_question_id: true,
                answer: true,
              },
            },
            badges: {
              select: {
                badge_id: true,
              },
            },
          },
        })

        args.flagQuestionAutoGrade.forEach((flagQuestionAutoGradeArgs) => {
          const correctAnswer = result.flagQuestions.filter(
            (shortAnswerQuestionRes) =>
              shortAnswerQuestionRes.flag_question_id ===
              flagQuestionAutoGradeArgs.flagQuestionId,
          )[0].answer

          score +=
            correctAnswer === flagQuestionAutoGradeArgs.answer &&
            !flagQuestionAutoGradeArgs.hint
              ? 1
              : 0
        })
        score /= args.flagQuestionAutoGrade.length
      }

      const time_left =
        result.max_time -
        (args.endTime.getTime() - args.startTime.getTime()) / 1000

      const time_score = time_left > 0 ? time_left * result.max_cost : 0
      const base_score = score * result.scorce

      if (base_score > 0) {
        const user_score = await this.prisma.userScores.create({
          data: {
            users: { connect: { user_id: user.user_id } },
            challenges: { connect: { challenge_id: args.challengeId } },
            base_score: base_score,
            time_score: time_score,
          },
        })

        if (user_score instanceof Error)
          return new InternalServerErrorException(user_score.message)

        const bag_result = await this.prisma.userBadges.create({
          data: {
            users: { connect: { user_id: user.user_id } },
            badges: { connect: { badge_id: result.badges.badge_id } },
          },
        })

        if (bag_result instanceof Error)
          return new InternalServerErrorException(bag_result.message)
      }

      return true
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
