import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as TeamModel from 'src/models/teams'
import * as UsersModel from 'src/models/users'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'
import { MinioClientService } from 'src/providers/storage/minio/minio.service'

@Injectable()
export class TeamsService {
  constructor(
    private prisma: PrismaService,
    private minioClientService: MinioClientService,
  ) {}

  // Bug
  async create(data: any): Promise<TeamModel.Teams | Error> {
    /**
     * Create Team record
     */
    try {
      // Insert Team record to database
      return await this.prisma.teams.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: TeamModel.FindManyTeamsArgs,
    select: any,
  ): Promise<TeamModel.Teams[] | Error> {
    /**
     * Get Team records
     */
    try {
      // Get Team records from database
      const result = await this.prisma.teams.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async getOne(
    args: TeamModel.FindManyTeamsArgs,
    select: any,
  ): Promise<TeamModel.Teams | Error> {
    /**
     * Get User records
     */
    try {
      // Get one Users record from database
      return await this.prisma.teams.findFirst({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: TeamModel.UpdateOneTeamsArgs,
    select: any,
    user: UsersModel.Users,
  ): Promise<TeamModel.Teams | Error> {
    /**
     * Update or create Users logging record
     */
    try {
      const result: any = await this.prisma.teamMembers.findFirst({
        where: {
          user_id: user.user_id,
        },
        select: {
          type: true,
          team_id: true,
        },
      })

      if (result.type !== 'captain')
        return new ForbiddenException('Can not update team')

      args.where.team_id = result.team_id
      // Update Team record
      return await this.prisma.teams.update({ ...args, ...select })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: TeamModel.DeleteOneTeamsArgs,
  ): Promise<TeamModel.Teams | Error> {
    /**
     * Delete Users record
     */
    try {
      // Delete User record
      return await this.prisma.teams.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async updateImage(
    args: TeamModel.UpdateOneTeamImageArgs,
    select: any,
    user: UsersModel.Users,
  ): Promise<TeamModel.Teams | Error> {
    /**
     * Update or create Cameras record
     */
    try {
      const result: any = await this.prisma.teamMembers.findFirst({
        where: {
          user_id: user.user_id,
        },
        select: {
          type: true,
          team_id: true,
        },
      })

      if (result.type !== 'captain')
        return new ForbiddenException('Can not update team')

      args.where.team_id = result.team_id
      const argsUser: TeamModel.FindManyTeamsArgs = {
        where: { team_id: { equals: args.where.team_id } },
      }
      const selectUser: any = {
        select: {
          team_id: true,
          team_profile_pic: true,
        },
      }
      const resultTeam = await this.getOne(argsUser, selectUser)
      if (resultTeam instanceof Error) return resultTeam

      if (
        resultTeam.team_profile_pic !== '' &&
        resultTeam.team_profile_pic !== null
      ) {
        const resultDelete = await this.minioClientService.delete(
          resultTeam.team_profile_pic,
        )
        if (resultDelete instanceof Error) return resultDelete
      }

      // Convert base64 to buffer
      const fileName: any = await this.minioClientService.upload(
        args.data.team_profile_pic,
      )
      if (fileName instanceof Error) return fileName

      args.data.team_profile_pic = fileName

      return await this.prisma.teams.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
