import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as EventsModel from 'src/models/events'
import * as UsersModel from 'src/models/users'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class EventsService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<EventsModel.Events | Error> {
    /**
     * Create event record
     */
    try {
      // Insert event record to database
      return await this.prisma.events.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: EventsModel.FindManyEventsArgs,
    select: any,
  ): Promise<EventsModel.Events[] | Error> {
    /**
     * Get event records
     */
    try {
      // Get event records from database
      const result = await this.prisma.events.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: EventsModel.UpdateOneEventsArgs,
    select: any,
  ): Promise<EventsModel.Events | Error> {
    /**
     * Update or create event record
     */
    try {
      // Update event record
      return await this.prisma.events.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: EventsModel.DeleteOneEventsArgs,
  ): Promise<EventsModel.Events | Error> {
    /**
     * Delete event record
     */
    try {
      // Delete event record
      return await this.prisma.events.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async autoGrade(
    args: EventsModel.EventAutoGrade,
    user: UsersModel.Users,
  ): Promise<boolean | Error> {
    /**
     * Auto grade event record
     */
    try {
      const result = await this.prisma.events.findFirst({
        where: {
          event_id: args.eventId,
        },
        select: {
          answer: true,
          badges: {
            select: {
              badge_id: true,
            },
          },
        },
      })

      if (result.answer === args.answer) {
        const bag_result = await this.prisma.userBadges.create({
          data: {
            users: { connect: { user_id: user.user_id } },
            badges: { connect: { badge_id: result.badges.badge_id } },
          },
        })

        if (bag_result instanceof Error)
          return new InternalServerErrorException(bag_result.message)
      }

      return true
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
