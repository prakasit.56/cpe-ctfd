import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as UserTutorialModel from 'src/models/user-tutorials'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class UserTutorialsService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<UserTutorialModel.UserTutorials | Error> {
    /**
     * Create user tutorial record
     */
    try {
      // Insert user tutorial record to database
      return await this.prisma.userTutorials.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: UserTutorialModel.FindManyUserTutorialsArgs,
    select: any,
  ): Promise<UserTutorialModel.UserTutorials[] | Error> {
    /**
     * Get user tutorial records
     */
    try {
      // Get user tutorial records from database
      const result = await this.prisma.userTutorials.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: UserTutorialModel.UpdateOneUserTutorialsArgs,
    select: any,
  ): Promise<UserTutorialModel.UserTutorials | Error> {
    /**
     * Update or create user tutorial record
     */
    try {
      // Update user tutorial record
      return await this.prisma.userTutorials.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: UserTutorialModel.DeleteOneUserTutorialsArgs,
  ): Promise<UserTutorialModel.UserTutorials | Error> {
    /**
     * Delete user tutorial record
     */
    try {
      // Delete user tutorial record
      return await this.prisma.userTutorials.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
