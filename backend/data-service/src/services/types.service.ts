import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as TypesModel from 'src/models/types'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class TypesService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<TypesModel.Types | Error> {
    /**
     * Create types record
     */
    try {
      // Insert types record to database
      return await this.prisma.types.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: TypesModel.FindManyTypesArgs,
    select: any,
  ): Promise<TypesModel.Types[] | Error> {
    /**
     * Get types records
     */
    try {
      // Get types records from database
      const result = await this.prisma.types.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: TypesModel.UpdateOneTypesArgs,
    select: any,
  ): Promise<TypesModel.Types | Error> {
    /**
     * Update or create types record
     */
    try {
      // Update types record
      return await this.prisma.types.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: TypesModel.DeleteOneTypesArgs,
  ): Promise<TypesModel.Types | Error> {
    /**
     * Delete types record
     */
    try {
      // Delete types record
      return await this.prisma.types.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
