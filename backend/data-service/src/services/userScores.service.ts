import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as UserScoresModel from 'src/models/user-scores'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class UserScoresService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<UserScoresModel.UserScores | Error> {
    /**
     * Create user scores record
     */
    try {
      // Insert user scores record to database
      return await this.prisma.userScores.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: UserScoresModel.FindManyUserScoresArgs,
    select: any,
  ): Promise<UserScoresModel.UserScores[] | Error> {
    /**
     * Get user scores records
     */
    try {
      // Get user scores records from database
      const result = await this.prisma.userScores.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: UserScoresModel.UpdateOneUserScoresArgs,
    select: any,
  ): Promise<UserScoresModel.UserScores | Error> {
    /**
     * Update or create user scores record
     */
    try {
      // Update user scores record
      return await this.prisma.userScores.update({
        ...args,
        ...select,
      })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: UserScoresModel.DeleteOneUserScoresArgs,
  ): Promise<UserScoresModel.UserScores | Error> {
    /**
     * Delete user scores record
     */
    try {
      // Delete user scores record
      return await this.prisma.userScores.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async groupBy(
    args: UserScoresModel.UserScoresGroupByLeaderBoardArgs,
  ): Promise<UserScoresModel.UserScoresGroupByForTotalScore[] | Error> {
    /**
     * groupby user scores record
     */
    try {
      let result: UserScoresModel.UserScoresGroupByForTotalScore[]
      if (
        args.by[0] ===
        UserScoresModel.UserScoresScalarFieldLeaderBoardEnum.user_id_total
      )
        result = await this.prisma.$queryRaw(
          Prisma.sql`
          SELECT u.name as "name", SUM(base_score + time_score) AS "totalScore", count(*) as "numChallenge"
          FROM "TBL_USER_SCORES" as us
          INNER JOIN "TBL_USERS" as u
          ON us.user_id = u.user_id
          GROUP BY u.name
          ORDER BY "totalScore" DESC
          `,
        )

      if (
        args.by[0] ===
        UserScoresModel.UserScoresScalarFieldLeaderBoardEnum.user_id_time
      )
        result = await this.prisma.$queryRaw(
          Prisma.sql`
            SELECT u.name as "name", SUM(time_score) AS "timeScore", count(*) as "numChallenge"
            FROM "TBL_USER_SCORES" as us
            INNER JOIN "TBL_USERS" as u
            ON us.user_id = u.user_id
            GROUP BY u.name
            ORDER BY "timeScore" DESC
          `,
        )

      if (
        args.by[0] ===
        UserScoresModel.UserScoresScalarFieldLeaderBoardEnum.team_id
      )
        result = await this.prisma.$queryRaw(
          Prisma.sql`
          SELECT t.name as "name", COALESCE(SUM(time_score + base_score),0) AS "totalScore", count(us.time_score) as "numChallenge"
          FROM "TBL_TEAMS" as t
          LEFT JOIN "TBL_TEAM_MEMBERS" as tm
          ON t.team_id = tm.team_id
          LEFT JOIN "TBL_USERS" as u
          ON tm.user_id = u.user_id
          LEFT JOIN "TBL_USER_SCORES" as us
          ON u.user_id = us.user_id
          GROUP BY t.name
          ORDER BY "totalScore" DESC
        `,
        )

      return result
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
