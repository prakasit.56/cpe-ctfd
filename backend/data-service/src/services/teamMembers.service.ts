import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common'
import { Prisma } from '@prisma/client'
import * as TeamMembersModel from 'src/models/team-members'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class TeamMembersService {
  constructor(private prisma: PrismaService) {}

  async create(data: any): Promise<TeamMembersModel.TeamMembers | Error> {
    /**
     * Create Team Member records
     */
    try {
      // Insert TeamMembers record to database
      return await this.prisma.teamMembers.create({ ...data })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2002') return new ForbiddenException(e.message)
        if (e.code === 'P2000') return new ConflictException(e.message)
        if (e.code === 'P2025') return new NotFoundException(e.message)
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async get(
    args: TeamMembersModel.FindManyTeamMembersArgs,
    select: any,
  ): Promise<TeamMembersModel.TeamMembers[] | Error> {
    /**
     * Get Team Member records
     */
    try {
      // Get Team Member records from database
      const result = await this.prisma.teamMembers.findMany({
        ...args,
        ...select,
      })

      if (!result.length) return new NotFoundException('Record not found')

      return result
    } catch (e) {
      if (e instanceof Error) return e
      return new InternalServerErrorException(e.message)
    }
  }

  async update(
    args: TeamMembersModel.UpdateOneTeamMembersArgs,
    select: any,
  ): Promise<TeamMembersModel.TeamMembers | Error> {
    /**
     * Update or create Team Member record
     */
    try {
      // Update Team Member record
      return await this.prisma.teamMembers.update({ ...args, ...select })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }

  async delete(
    args: TeamMembersModel.DeleteOneTeamMembersArgs,
  ): Promise<TeamMembersModel.TeamMembers | Error> {
    /**
     * Delete Team Member record
     */
    try {
      // Delete User record
      return await this.prisma.teamMembers.delete({ ...args })
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        // The .code property can be accessed in a type-safe manner
        if (e.code === 'P2025')
          return new NotFoundException(Object.values(e.meta)[0])
      }
      return new InternalServerErrorException(e.message)
    }
  }
}
