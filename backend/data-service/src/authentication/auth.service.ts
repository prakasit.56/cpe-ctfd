import * as bcrypt from 'bcrypt'
import { Logger, Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import * as UsersModel from 'src/models/users'
import { PrismaService } from 'src/providers/databases/prisma/prisma.service'

@Injectable()
export class AuthService {
  constructor(
    private logger: Logger,
    private readonly jwtService: JwtService,
    private config: ConfigService,
    private prisma: PrismaService,
  ) {
    this.logger = new Logger('Authentication Service')
  }

  hash(password: string): Promise<string> {
    return bcrypt.hash(password, 10)
  }

  compare(password: string, hash: string): Promise<boolean> {
    return bcrypt.compare(password, hash)
  }

  async generate(
    user: UsersModel.UsersWithoutPassword,
  ): Promise<{ access_token: string; refresh_token: string }> {
    const jwtPayload = {
      name: user.name,
      sub: user.user_id,
    }

    const [at, rt] = await Promise.all([
      this.jwtService.signAsync(jwtPayload, {
        secret: this.config.get<string>('JWT.SECRET_KEY'),
        expiresIn: '5m',
      }),
      this.jwtService.signAsync(jwtPayload, {
        secret: this.config.get<string>('JWT.REFRESH_SECRET_KEY'),
        expiresIn: '14d',
      }),
    ])

    return {
      access_token: at,
      refresh_token: rt,
    }
  }

  async verify(
    data: {
      name: string
      sub: string
      iat: number
    },
    bypass = false,
  ): Promise<UsersModel.Users> {
    // Check user is exist
    let where: UsersModel.UsersWhereInput = {
      user_id: { equals: data.sub },
    }
    if (bypass) {
      where = {
        user_role: { is: { name: { equals: 'ADMIN' } } },
      }
    }

    const select: any = {
      select: {
        user_id: true,
        name: true,
        email: true,
        user_role: { select: { name: true } },
      },
    }

    // Get User records from database
    const result = await this.prisma.users.findFirst({
      where,
      ...select,
    })

    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
