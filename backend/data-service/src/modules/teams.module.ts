import { Module } from '@nestjs/common'
import { TeamsService } from 'src/services/teams.service'
import { TeamsResolver } from 'src/resolvers/teams.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'
import { MinioClientModule } from 'src/providers/storage/minio/minio.module'

@Module({
  imports: [PrismaModule, MinioClientModule],
  providers: [TeamsService, TeamsResolver],
})
export class TeamsModule {}
