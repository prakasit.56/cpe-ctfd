import { Module } from '@nestjs/common'
import { UserTutorialsService } from 'src/services/userTutorials.service'
import { UserTutorialsResolver } from 'src/resolvers/userTutorials.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [UserTutorialsService, UserTutorialsResolver],
})
export class UserTutorialsModule {}
