import { Module } from '@nestjs/common'
import { BadgesService } from 'src/services/badges.service'
import { BadgesResolver } from 'src/resolvers/badges.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [BadgesResolver, BadgesService],
})
export class BadgesModule {}
