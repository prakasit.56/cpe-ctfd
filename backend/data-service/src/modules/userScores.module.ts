import { Module } from '@nestjs/common'
import { UserScoresService } from 'src/services/userScores.service'
import { UserScoresResolver } from 'src/resolvers/userScores.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [UserScoresResolver, UserScoresService],
})
export class UserScoresModule {}
