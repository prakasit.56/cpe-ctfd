import { Module } from '@nestjs/common'
import { MultipleQuestionsService } from 'src/services/multipleQuestions.service'
import { MutipleQuestionsResolver } from 'src/resolvers/mutipleQuestions.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [MultipleQuestionsService, MutipleQuestionsResolver],
})
export class MultipleQuestionsModule {}
