import { Module } from '@nestjs/common'
import { ShortAnswerQuestionsService } from 'src/services/shortAnswerQuestions.service'
import { ShortAnswerQuestionsResolver } from 'src/resolvers/shortAnswerQuestions.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [ShortAnswerQuestionsService, ShortAnswerQuestionsResolver],
})
export class ShortAnswerQuestionsModule {}
