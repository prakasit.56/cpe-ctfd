import { Module } from '@nestjs/common'
import { TutorialChapterDatasService } from 'src/services/tutorialChapterDatas.service'
import { TutorialChapterDatasResolver } from 'src/resolvers/tutorialChapterDatas.resolvers'
import { PrismaModule } from 'src/providers/databases/prisma/prisma.module'

@Module({
  imports: [PrismaModule],
  providers: [TutorialChapterDatasService, TutorialChapterDatasResolver],
})
export class TutorialChapterDatasModule {}
