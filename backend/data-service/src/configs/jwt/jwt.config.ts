export const SECRET_KEY = process.env.JWT_SECRET_KEY
export const REFRESH_SECRET_KEY = process.env.JWT_REFRESH_SECRET_KEY
export const BYPASS_SECRET_KEY = process.env.BYPASS_SECRET_KEY
