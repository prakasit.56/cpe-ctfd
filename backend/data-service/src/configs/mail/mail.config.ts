export const MAIL_CONFIG = {
  HOST: process.env.MAIL_HOST,
  PORT: 465,
  SECURE: true,
  USER: process.env.MAIL_USER,
  PASS: process.env.MAIL_PASS,
}
