import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensUpdateManyMutationInput } from './user-tokens-update-many-mutation.input'
import { UserTokensWhereInput } from './user-tokens-where.input'

@ArgsType()
export class UpdateManyUserTokensArgs {
  @Field(() => UserTokensUpdateManyMutationInput, { nullable: false })
  data!: UserTokensUpdateManyMutationInput

  @Field(() => UserTokensWhereInput, { nullable: true })
  where?: UserTokensWhereInput
}
