import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensWhereInput } from './user-tokens-where.input'

@ArgsType()
export class DeleteManyUserTokensArgs {
  @Field(() => UserTokensWhereInput, { nullable: true })
  where?: UserTokensWhereInput
}
