import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensWhereInput } from './user-tokens-where.input'
import { UserTokensOrderByWithAggregationInput } from './user-tokens-order-by-with-aggregation.input'
import { UserTokensScalarFieldEnum } from './user-tokens-scalar-field.enum'
import { UserTokensScalarWhereWithAggregatesInput } from './user-tokens-scalar-where-with-aggregates.input'
import { Int } from '@nestjs/graphql'
import { UserTokensCountAggregateInput } from './user-tokens-count-aggregate.input'
import { UserTokensMinAggregateInput } from './user-tokens-min-aggregate.input'
import { UserTokensMaxAggregateInput } from './user-tokens-max-aggregate.input'

@ArgsType()
export class UserTokensGroupByArgs {
  @Field(() => UserTokensWhereInput, { nullable: true })
  where?: UserTokensWhereInput

  @Field(() => [UserTokensOrderByWithAggregationInput], { nullable: true })
  orderBy?: Array<UserTokensOrderByWithAggregationInput>

  @Field(() => [UserTokensScalarFieldEnum], { nullable: false })
  by!: Array<keyof typeof UserTokensScalarFieldEnum>

  @Field(() => UserTokensScalarWhereWithAggregatesInput, { nullable: true })
  having?: UserTokensScalarWhereWithAggregatesInput

  @Field(() => Int, { nullable: true })
  take?: number

  @Field(() => Int, { nullable: true })
  skip?: number

  @Field(() => UserTokensCountAggregateInput, { nullable: true })
  _count?: UserTokensCountAggregateInput

  @Field(() => UserTokensMinAggregateInput, { nullable: true })
  _min?: UserTokensMinAggregateInput

  @Field(() => UserTokensMaxAggregateInput, { nullable: true })
  _max?: UserTokensMaxAggregateInput
}
