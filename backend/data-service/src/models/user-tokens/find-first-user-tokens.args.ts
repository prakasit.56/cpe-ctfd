import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensWhereInput } from './user-tokens-where.input'
import { UserTokensOrderByWithRelationInput } from './user-tokens-order-by-with-relation.input'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'
import { Int } from '@nestjs/graphql'
import { UserTokensScalarFieldEnum } from './user-tokens-scalar-field.enum'

@ArgsType()
export class FindFirstUserTokensArgs {
  @Field(() => UserTokensWhereInput, { nullable: true })
  where?: UserTokensWhereInput

  @Field(() => [UserTokensOrderByWithRelationInput], { nullable: true })
  orderBy?: Array<UserTokensOrderByWithRelationInput>

  @Field(() => UserTokensWhereUniqueInput, { nullable: true })
  cursor?: UserTokensWhereUniqueInput

  @Field(() => Int, { nullable: true })
  take?: number

  @Field(() => Int, { nullable: true })
  skip?: number

  @Field(() => [UserTokensScalarFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof UserTokensScalarFieldEnum>
}
