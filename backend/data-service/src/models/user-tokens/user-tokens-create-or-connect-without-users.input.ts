import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'
import { UserTokensCreateWithoutUsersInput } from './user-tokens-create-without-users.input'

@InputType()
export class UserTokensCreateOrConnectWithoutUsersInput {
  @Field(() => UserTokensWhereUniqueInput, { nullable: false })
  where!: UserTokensWhereUniqueInput

  @Field(() => UserTokensCreateWithoutUsersInput, { nullable: false })
  create!: UserTokensCreateWithoutUsersInput
}
