import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringWithAggregatesFilter } from '../prisma/string-with-aggregates-filter.input'
import { DateTimeWithAggregatesFilter } from '../prisma/date-time-with-aggregates-filter.input'

@InputType()
export class UserTokensScalarWhereWithAggregatesInput {
  @Field(() => [UserTokensScalarWhereWithAggregatesInput], { nullable: true })
  AND?: Array<UserTokensScalarWhereWithAggregatesInput>

  @Field(() => [UserTokensScalarWhereWithAggregatesInput], { nullable: true })
  OR?: Array<UserTokensScalarWhereWithAggregatesInput>

  @Field(() => [UserTokensScalarWhereWithAggregatesInput], { nullable: true })
  NOT?: Array<UserTokensScalarWhereWithAggregatesInput>

  @Field(() => StringWithAggregatesFilter, { nullable: true })
  token_id?: StringWithAggregatesFilter

  @Field(() => StringWithAggregatesFilter, { nullable: true })
  user_id?: StringWithAggregatesFilter

  @Field(() => StringWithAggregatesFilter, { nullable: true })
  token?: StringWithAggregatesFilter

  @Field(() => DateTimeWithAggregatesFilter, { nullable: true })
  createdAt?: DateTimeWithAggregatesFilter

  @Field(() => DateTimeWithAggregatesFilter, { nullable: true })
  updatedAt?: DateTimeWithAggregatesFilter
}
