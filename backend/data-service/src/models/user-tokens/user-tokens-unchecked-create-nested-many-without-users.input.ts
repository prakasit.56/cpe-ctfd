import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensCreateWithoutUsersInput } from './user-tokens-create-without-users.input'
import { UserTokensCreateOrConnectWithoutUsersInput } from './user-tokens-create-or-connect-without-users.input'
import { UserTokensCreateManyUsersInputEnvelope } from './user-tokens-create-many-users-input-envelope.input'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'

@InputType()
export class UserTokensUncheckedCreateNestedManyWithoutUsersInput {
  @Field(() => [UserTokensCreateWithoutUsersInput], { nullable: true })
  create?: Array<UserTokensCreateWithoutUsersInput>

  @Field(() => [UserTokensCreateOrConnectWithoutUsersInput], { nullable: true })
  connectOrCreate?: Array<UserTokensCreateOrConnectWithoutUsersInput>

  @Field(() => UserTokensCreateManyUsersInputEnvelope, { nullable: true })
  createMany?: UserTokensCreateManyUsersInputEnvelope

  @Field(() => [UserTokensWhereUniqueInput], { nullable: true })
  connect?: Array<UserTokensWhereUniqueInput>
}
