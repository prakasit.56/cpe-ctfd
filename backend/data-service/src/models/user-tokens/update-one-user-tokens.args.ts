import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserTokensUpdateInput } from './user-tokens-update.input'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'

@ArgsType()
export class UpdateOneUserTokensArgs {
  @Field(() => UserTokensUpdateInput, { nullable: false })
  data!: UserTokensUpdateInput

  @Field(() => UserTokensWhereUniqueInput, { nullable: false })
  where!: UserTokensWhereUniqueInput
}
