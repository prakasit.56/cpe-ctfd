import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersCreateNestedOneWithoutUserTokensInput } from '../users/users-create-nested-one-without-user-tokens.input'

@InputType()
export class UserTokensCreateInput {
  @Field(() => String, { nullable: true })
  token_id?: string

  @Field(() => UsersCreateNestedOneWithoutUserTokensInput, { nullable: false })
  users!: UsersCreateNestedOneWithoutUserTokensInput

  @Field(() => String, { nullable: false })
  token!: string

  @Field(() => Date, { nullable: true })
  createdAt?: Date | string

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | string
}
