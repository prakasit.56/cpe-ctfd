import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'
import { UserTokensUpdateWithoutUsersInput } from './user-tokens-update-without-users.input'
import { UserTokensCreateWithoutUsersInput } from './user-tokens-create-without-users.input'

@InputType()
export class UserTokensUpsertWithWhereUniqueWithoutUsersInput {
  @Field(() => UserTokensWhereUniqueInput, { nullable: false })
  where!: UserTokensWhereUniqueInput

  @Field(() => UserTokensUpdateWithoutUsersInput, { nullable: false })
  update!: UserTokensUpdateWithoutUsersInput

  @Field(() => UserTokensCreateWithoutUsersInput, { nullable: false })
  create!: UserTokensCreateWithoutUsersInput
}
