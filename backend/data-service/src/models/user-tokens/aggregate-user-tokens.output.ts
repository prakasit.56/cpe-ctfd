import { Field } from '@nestjs/graphql'
import { ObjectType } from '@nestjs/graphql'
import { UserTokensCountAggregate } from './user-tokens-count-aggregate.output'
import { UserTokensMinAggregate } from './user-tokens-min-aggregate.output'
import { UserTokensMaxAggregate } from './user-tokens-max-aggregate.output'

@ObjectType()
export class AggregateUserTokens {
  @Field(() => UserTokensCountAggregate, { nullable: true })
  _count?: UserTokensCountAggregate

  @Field(() => UserTokensMinAggregate, { nullable: true })
  _min?: UserTokensMinAggregate

  @Field(() => UserTokensMaxAggregate, { nullable: true })
  _max?: UserTokensMaxAggregate
}
