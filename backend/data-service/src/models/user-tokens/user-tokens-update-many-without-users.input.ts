import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserTokensCreateWithoutUsersInput } from './user-tokens-create-without-users.input'
import { UserTokensCreateOrConnectWithoutUsersInput } from './user-tokens-create-or-connect-without-users.input'
import { UserTokensUpsertWithWhereUniqueWithoutUsersInput } from './user-tokens-upsert-with-where-unique-without-users.input'
import { UserTokensCreateManyUsersInputEnvelope } from './user-tokens-create-many-users-input-envelope.input'
import { UserTokensWhereUniqueInput } from './user-tokens-where-unique.input'
import { UserTokensUpdateWithWhereUniqueWithoutUsersInput } from './user-tokens-update-with-where-unique-without-users.input'
import { UserTokensUpdateManyWithWhereWithoutUsersInput } from './user-tokens-update-many-with-where-without-users.input'
import { UserTokensScalarWhereInput } from './user-tokens-scalar-where.input'

@InputType()
export class UserTokensUpdateManyWithoutUsersInput {
  @Field(() => [UserTokensCreateWithoutUsersInput], { nullable: true })
  create?: Array<UserTokensCreateWithoutUsersInput>

  @Field(() => [UserTokensCreateOrConnectWithoutUsersInput], { nullable: true })
  connectOrCreate?: Array<UserTokensCreateOrConnectWithoutUsersInput>

  @Field(() => [UserTokensUpsertWithWhereUniqueWithoutUsersInput], {
    nullable: true,
  })
  upsert?: Array<UserTokensUpsertWithWhereUniqueWithoutUsersInput>

  @Field(() => UserTokensCreateManyUsersInputEnvelope, { nullable: true })
  createMany?: UserTokensCreateManyUsersInputEnvelope

  @Field(() => [UserTokensWhereUniqueInput], { nullable: true })
  set?: Array<UserTokensWhereUniqueInput>

  @Field(() => [UserTokensWhereUniqueInput], { nullable: true })
  disconnect?: Array<UserTokensWhereUniqueInput>

  @Field(() => [UserTokensWhereUniqueInput], { nullable: true })
  delete?: Array<UserTokensWhereUniqueInput>

  @Field(() => [UserTokensWhereUniqueInput], { nullable: true })
  connect?: Array<UserTokensWhereUniqueInput>

  @Field(() => [UserTokensUpdateWithWhereUniqueWithoutUsersInput], {
    nullable: true,
  })
  update?: Array<UserTokensUpdateWithWhereUniqueWithoutUsersInput>

  @Field(() => [UserTokensUpdateManyWithWhereWithoutUsersInput], {
    nullable: true,
  })
  updateMany?: Array<UserTokensUpdateManyWithWhereWithoutUsersInput>

  @Field(() => [UserTokensScalarWhereInput], { nullable: true })
  deleteMany?: Array<UserTokensScalarWhereInput>
}
