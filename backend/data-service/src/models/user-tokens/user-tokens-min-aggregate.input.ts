import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'

@InputType()
export class UserTokensMinAggregateInput {
  @Field(() => Boolean, { nullable: true })
  token_id?: true

  @Field(() => Boolean, { nullable: true })
  user_id?: true

  @Field(() => Boolean, { nullable: true })
  token?: true

  @Field(() => Boolean, { nullable: true })
  createdAt?: true

  @Field(() => Boolean, { nullable: true })
  updatedAt?: true
}
