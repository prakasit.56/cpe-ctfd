import { registerEnumType } from '@nestjs/graphql'

export enum UserTokensScalarFieldEnum {
  token_id = 'token_id',
  user_id = 'user_id',
  token = 'token',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

registerEnumType(UserTokensScalarFieldEnum, {
  name: 'UserTokensScalarFieldEnum',
  description: undefined,
})
