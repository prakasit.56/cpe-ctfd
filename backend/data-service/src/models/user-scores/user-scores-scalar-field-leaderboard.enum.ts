import { registerEnumType } from '@nestjs/graphql'

export enum UserScoresScalarFieldLeaderBoardEnum {
  team_id = 'team_id',
  user_id_total = 'user_id_total',
  user_id_time = 'user_id_time',
}

registerEnumType(UserScoresScalarFieldLeaderBoardEnum, {
  name: 'UserScoresGroupByLeaderBoardArgs',
  description: undefined,
})
