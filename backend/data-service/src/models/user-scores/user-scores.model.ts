import { Field } from '@nestjs/graphql'
import { ObjectType } from '@nestjs/graphql'
import { Users } from '../users/users.model'
import { Challenges } from '../challenges/challenges.model'
import { Int } from '@nestjs/graphql'
import { Float } from '@nestjs/graphql'
import { Levels } from '../levels/levels.model'
import { Types } from '../types/types.model'
import { Categories } from '../categories/categories.model'

@ObjectType()
export class UserScores {
  @Field(() => Users, { nullable: false })
  users?: Users

  @Field(() => String, { nullable: false })
  user_id!: string

  @Field(() => Challenges, { nullable: false })
  challenges?: Challenges

  @Field(() => String, { nullable: false })
  challenge_id!: string

  @Field(() => Int, { nullable: false, defaultValue: 0 })
  base_score!: number

  @Field(() => Float, { nullable: false, defaultValue: 0 })
  time_score!: number

  @Field(() => Levels, { nullable: true })
  levels?: Levels | null

  @Field(() => String, { nullable: true })
  level_id!: string | null

  @Field(() => Types, { nullable: true })
  types?: Types | null

  @Field(() => String, { nullable: true })
  types_id!: string | null

  @Field(() => Categories, { nullable: true })
  categories?: Categories | null

  @Field(() => String, { nullable: true })
  categories_id!: string | null

  @Field(() => Date, { nullable: false })
  createdAt!: Date

  @Field(() => Date, { nullable: false })
  updatedAt!: Date
}
