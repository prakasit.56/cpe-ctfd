import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringFilter } from '../prisma/string-filter.input'
import { IntFilter } from '../prisma/int-filter.input'
import { FloatFilter } from '../prisma/float-filter.input'
import { StringNullableFilter } from '../prisma/string-nullable-filter.input'
import { DateTimeFilter } from '../prisma/date-time-filter.input'

@InputType()
export class UserScoresScalarWhereInput {
  @Field(() => [UserScoresScalarWhereInput], { nullable: true })
  AND?: Array<UserScoresScalarWhereInput>

  @Field(() => [UserScoresScalarWhereInput], { nullable: true })
  OR?: Array<UserScoresScalarWhereInput>

  @Field(() => [UserScoresScalarWhereInput], { nullable: true })
  NOT?: Array<UserScoresScalarWhereInput>

  @Field(() => StringFilter, { nullable: true })
  user_id?: StringFilter

  @Field(() => StringFilter, { nullable: true })
  challenge_id?: StringFilter

  @Field(() => IntFilter, { nullable: true })
  base_score?: IntFilter

  @Field(() => FloatFilter, { nullable: true })
  time_score?: FloatFilter

  @Field(() => StringNullableFilter, { nullable: true })
  level_id?: StringNullableFilter

  @Field(() => StringNullableFilter, { nullable: true })
  types_id?: StringNullableFilter

  @Field(() => StringNullableFilter, { nullable: true })
  categories_id?: StringNullableFilter

  @Field(() => DateTimeFilter, { nullable: true })
  createdAt?: DateTimeFilter

  @Field(() => DateTimeFilter, { nullable: true })
  updatedAt?: DateTimeFilter
}
