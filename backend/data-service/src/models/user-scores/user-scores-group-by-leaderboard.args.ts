import { UserScoresScalarFieldLeaderBoardEnum } from './user-scores-scalar-field-leaderboard.enum'
import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'

@ArgsType()
export class UserScoresGroupByLeaderBoardArgs {
  @Field(() => [UserScoresScalarFieldLeaderBoardEnum], { nullable: false })
  by!: Array<keyof typeof UserScoresScalarFieldLeaderBoardEnum>
}
