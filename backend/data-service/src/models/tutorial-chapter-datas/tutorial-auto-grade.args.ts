import { Field, ArgsType, ID } from '@nestjs/graphql'

@ArgsType()
export class TutorialAutoGrade {
  @Field(() => ID, { nullable: false })
  chapterId!: string

  @Field(() => String, { nullable: false })
  answer!: string
}
