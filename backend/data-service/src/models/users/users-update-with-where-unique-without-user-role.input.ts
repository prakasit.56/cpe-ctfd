import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersWhereUniqueInput } from './users-where-unique.input'
import { UsersUpdateWithoutUser_roleInput } from './users-update-without-user-role.input'

@InputType()
export class UsersUpdateWithWhereUniqueWithoutUser_roleInput {
  @Field(() => UsersWhereUniqueInput, { nullable: false })
  where!: UsersWhereUniqueInput

  @Field(() => UsersUpdateWithoutUser_roleInput, { nullable: false })
  data!: UsersUpdateWithoutUser_roleInput
}
