import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersUpdateWithoutUserTokensInput } from './users-update-without-user-tokens.input'
import { UsersCreateWithoutUserTokensInput } from './users-create-without-user-tokens.input'

@InputType()
export class UsersUpsertWithoutUserTokensInput {
  @Field(() => UsersUpdateWithoutUserTokensInput, { nullable: false })
  update!: UsersUpdateWithoutUserTokensInput

  @Field(() => UsersCreateWithoutUserTokensInput, { nullable: false })
  create!: UsersCreateWithoutUserTokensInput
}
