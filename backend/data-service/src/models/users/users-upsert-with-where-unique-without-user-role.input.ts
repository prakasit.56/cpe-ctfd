import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersWhereUniqueInput } from './users-where-unique.input'
import { UsersUpdateWithoutUser_roleInput } from './users-update-without-user-role.input'
import { UsersCreateWithoutUser_roleInput } from './users-create-without-user-role.input'

@InputType()
export class UsersUpsertWithWhereUniqueWithoutUser_roleInput {
  @Field(() => UsersWhereUniqueInput, { nullable: false })
  where!: UsersWhereUniqueInput

  @Field(() => UsersUpdateWithoutUser_roleInput, { nullable: false })
  update!: UsersUpdateWithoutUser_roleInput

  @Field(() => UsersCreateWithoutUser_roleInput, { nullable: false })
  create!: UsersCreateWithoutUser_roleInput
}
