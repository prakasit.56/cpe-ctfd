import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersCreateWithoutUserTokensInput } from './users-create-without-user-tokens.input'
import { UsersCreateOrConnectWithoutUserTokensInput } from './users-create-or-connect-without-user-tokens.input'
import { UsersWhereUniqueInput } from './users-where-unique.input'

@InputType()
export class UsersCreateNestedOneWithoutUserTokensInput {
  @Field(() => UsersCreateWithoutUserTokensInput, { nullable: true })
  create?: UsersCreateWithoutUserTokensInput

  @Field(() => UsersCreateOrConnectWithoutUserTokensInput, { nullable: true })
  connectOrCreate?: UsersCreateOrConnectWithoutUserTokensInput

  @Field(() => UsersWhereUniqueInput, { nullable: true })
  connect?: UsersWhereUniqueInput
}
