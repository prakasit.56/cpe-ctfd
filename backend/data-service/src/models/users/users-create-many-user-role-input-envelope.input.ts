import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersCreateManyUser_roleInput } from './users-create-many-user-role.input'

@InputType()
export class UsersCreateManyUser_roleInputEnvelope {
  @Field(() => [UsersCreateManyUser_roleInput], { nullable: false })
  data!: Array<UsersCreateManyUser_roleInput>

  @Field(() => Boolean, { nullable: true })
  skipDuplicates?: boolean
}
