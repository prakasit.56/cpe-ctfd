import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class UsersLoginInput {
  @Field(() => String, { nullable: true })
  username: string

  @Field(() => String, { nullable: true })
  password: string
}
