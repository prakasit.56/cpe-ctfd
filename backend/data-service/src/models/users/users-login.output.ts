import { Field, ID, ObjectType, Float, Int } from '@nestjs/graphql'
import { UserLoggings } from 'src/models/user-loggings/user-loggings.model'
import { UserBadges } from 'src/models//user-badges/user-badges.model'
import { TeamMembers } from 'src/models//team-members/team-members.model'
import { UserTutorials } from 'src/models//user-tutorials/user-tutorials.model'
import { UserScores } from 'src/models//user-scores/user-scores.model'
import { UsersCount } from 'src/models/users/users-count.output'

@ObjectType()
export class UsersLoginOutput {
  @Field(() => ID, { nullable: false })
  user_id!: string

  @Field(() => Int, { nullable: false })
  user_invite_id!: number

  @Field(() => String, { nullable: true })
  profile_pic!: string

  @Field(() => Float, { nullable: false })
  total_score!: number

  @Field(() => String, { nullable: false })
  type!: string

  @Field(() => String, { nullable: false })
  name!: string

  @Field(() => String, { nullable: false })
  username!: string

  @Field(() => String, { nullable: false })
  email!: string

  @Field(() => String, { nullable: false })
  conuntry_code!: string

  @Field(() => Boolean, { nullable: false })
  banned!: boolean

  @Field(() => Boolean, { nullable: false })
  verified!: boolean

  @Field(() => Boolean, { nullable: false })
  quick_start_status!: boolean

  @Field(() => String, { nullable: false })
  profile_description!: string

  @Field(() => String, { nullable: false })
  github_link!: string

  @Field(() => String, { nullable: false })
  twitter_link!: string

  @Field(() => String, { nullable: false })
  facebook_link!: string

  @Field(() => Date, { nullable: false })
  createdAt!: Date

  @Field(() => Date, { nullable: false })
  updatedAt!: Date

  @Field(() => [UserLoggings], { nullable: true })
  userLoggings?: Array<UserLoggings>

  @Field(() => [UserBadges], { nullable: true })
  userBadges?: Array<UserBadges>

  @Field(() => [TeamMembers], { nullable: true })
  teamMembers?: Array<TeamMembers>

  @Field(() => [UserTutorials], { nullable: true })
  userTutorials?: Array<UserTutorials>

  @Field(() => [UserScores], { nullable: true })
  userScores?: Array<UserScores>

  @Field(() => UsersCount, { nullable: false })
  _count?: UsersCount

  @Field(() => String, { nullable: false })
  refresh_token?: string

  @Field(() => String, { nullable: false })
  access_token?: string
}
