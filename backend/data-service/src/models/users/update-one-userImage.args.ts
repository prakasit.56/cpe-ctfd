import { Field, ArgsType } from '@nestjs/graphql'
import { UserUpdateImageUserInput } from 'src/models/users/user-update-image.input'
import { UsersWhereUniqueInput } from 'src/models/users/users-where-unique.input'

@ArgsType()
export class UpdateOneUserImageArgs {
  @Field(() => UserUpdateImageUserInput, { nullable: false })
  data!: UserUpdateImageUserInput

  @Field(() => UsersWhereUniqueInput, { nullable: false })
  where!: UsersWhereUniqueInput
}
