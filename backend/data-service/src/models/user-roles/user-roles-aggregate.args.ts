import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesWhereInput } from './user-roles-where.input'
import { UserRolesOrderByWithRelationInput } from './user-roles-order-by-with-relation.input'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'
import { Int } from '@nestjs/graphql'
import { UserRolesCountAggregateInput } from './user-roles-count-aggregate.input'
import { UserRolesMinAggregateInput } from './user-roles-min-aggregate.input'
import { UserRolesMaxAggregateInput } from './user-roles-max-aggregate.input'

@ArgsType()
export class UserRolesAggregateArgs {
  @Field(() => UserRolesWhereInput, { nullable: true })
  where?: UserRolesWhereInput

  @Field(() => [UserRolesOrderByWithRelationInput], { nullable: true })
  orderBy?: Array<UserRolesOrderByWithRelationInput>

  @Field(() => UserRolesWhereUniqueInput, { nullable: true })
  cursor?: UserRolesWhereUniqueInput

  @Field(() => Int, { nullable: true })
  take?: number

  @Field(() => Int, { nullable: true })
  skip?: number

  @Field(() => UserRolesCountAggregateInput, { nullable: true })
  _count?: UserRolesCountAggregateInput

  @Field(() => UserRolesMinAggregateInput, { nullable: true })
  _min?: UserRolesMinAggregateInput

  @Field(() => UserRolesMaxAggregateInput, { nullable: true })
  _max?: UserRolesMaxAggregateInput
}
