import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersCreateNestedManyWithoutUser_roleInput } from '../users/users-create-nested-many-without-user-role.input'

@InputType()
export class UserRolesCreateInput {
  @Field(() => String, { nullable: true })
  user_role_id?: string

  @Field(() => String, { nullable: false })
  name!: string

  @Field(() => UsersCreateNestedManyWithoutUser_roleInput, { nullable: true })
  users?: UsersCreateNestedManyWithoutUser_roleInput

  @Field(() => Date, { nullable: true })
  createdAt?: Date | string

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | string
}
