import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesWhereInput } from './user-roles-where.input'
import { UserRolesOrderByWithRelationInput } from './user-roles-order-by-with-relation.input'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'
import { Int } from '@nestjs/graphql'
import { UserRolesScalarFieldEnum } from './user-roles-scalar-field.enum'

@ArgsType()
export class FindManyUserRolesArgs {
  @Field(() => UserRolesWhereInput, { nullable: true })
  where?: UserRolesWhereInput

  @Field(() => [UserRolesOrderByWithRelationInput], { nullable: true })
  orderBy?: Array<UserRolesOrderByWithRelationInput>

  @Field(() => UserRolesWhereUniqueInput, { nullable: true })
  cursor?: UserRolesWhereUniqueInput

  @Field(() => Int, { nullable: true })
  take?: number

  @Field(() => Int, { nullable: true })
  skip?: number

  @Field(() => [UserRolesScalarFieldEnum], { nullable: true })
  distinct?: Array<keyof typeof UserRolesScalarFieldEnum>
}
