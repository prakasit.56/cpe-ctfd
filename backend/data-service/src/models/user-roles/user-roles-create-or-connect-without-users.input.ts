import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UserRolesWhereUniqueInput } from './user-roles-where-unique.input'
import { UserRolesCreateWithoutUsersInput } from './user-roles-create-without-users.input'

@InputType()
export class UserRolesCreateOrConnectWithoutUsersInput {
  @Field(() => UserRolesWhereUniqueInput, { nullable: false })
  where!: UserRolesWhereUniqueInput

  @Field(() => UserRolesCreateWithoutUsersInput, { nullable: false })
  create!: UserRolesCreateWithoutUsersInput
}
