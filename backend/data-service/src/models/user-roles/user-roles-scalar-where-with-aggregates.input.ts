import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { StringWithAggregatesFilter } from '../prisma/string-with-aggregates-filter.input'
import { DateTimeWithAggregatesFilter } from '../prisma/date-time-with-aggregates-filter.input'

@InputType()
export class UserRolesScalarWhereWithAggregatesInput {
  @Field(() => [UserRolesScalarWhereWithAggregatesInput], { nullable: true })
  AND?: Array<UserRolesScalarWhereWithAggregatesInput>

  @Field(() => [UserRolesScalarWhereWithAggregatesInput], { nullable: true })
  OR?: Array<UserRolesScalarWhereWithAggregatesInput>

  @Field(() => [UserRolesScalarWhereWithAggregatesInput], { nullable: true })
  NOT?: Array<UserRolesScalarWhereWithAggregatesInput>

  @Field(() => StringWithAggregatesFilter, { nullable: true })
  user_role_id?: StringWithAggregatesFilter

  @Field(() => StringWithAggregatesFilter, { nullable: true })
  name?: StringWithAggregatesFilter

  @Field(() => DateTimeWithAggregatesFilter, { nullable: true })
  createdAt?: DateTimeWithAggregatesFilter

  @Field(() => DateTimeWithAggregatesFilter, { nullable: true })
  updatedAt?: DateTimeWithAggregatesFilter
}
