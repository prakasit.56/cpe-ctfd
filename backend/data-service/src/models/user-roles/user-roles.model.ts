import { Field } from '@nestjs/graphql'
import { ObjectType } from '@nestjs/graphql'
import { ID } from '@nestjs/graphql'
import { Users } from '../users/users.model'
import { UserRolesCount } from './user-roles-count.output'

@ObjectType()
export class UserRoles {
  @Field(() => ID, { nullable: false })
  user_role_id!: string

  @Field(() => String, { nullable: false })
  name!: string

  @Field(() => [Users], { nullable: true })
  users?: Array<Users>

  @Field(() => Date, { nullable: false })
  createdAt!: Date

  @Field(() => Date, { nullable: false })
  updatedAt!: Date

  @Field(() => UserRolesCount, { nullable: false })
  _count?: UserRolesCount
}
