import { Field } from '@nestjs/graphql'
import { ArgsType } from '@nestjs/graphql'
import { UserRolesCreateManyInput } from './user-roles-create-many.input'

@ArgsType()
export class CreateManyUserRolesArgs {
  @Field(() => [UserRolesCreateManyInput], { nullable: false })
  data!: Array<UserRolesCreateManyInput>

  @Field(() => Boolean, { nullable: true })
  skipDuplicates?: boolean
}
