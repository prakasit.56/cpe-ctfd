import { Field } from '@nestjs/graphql'
import { InputType } from '@nestjs/graphql'
import { UsersUpdateOneRequiredWithoutUserBadgesInput } from '../users/users-update-one-required-without-user-badges.input'
import { DateTimeFieldUpdateOperationsInput } from '../prisma/date-time-field-update-operations.input'

@InputType()
export class UserBadgesUpdateWithoutBadgesInput {
  @Field(() => UsersUpdateOneRequiredWithoutUserBadgesInput, { nullable: true })
  users?: UsersUpdateOneRequiredWithoutUserBadgesInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  createdAt?: DateTimeFieldUpdateOperationsInput

  @Field(() => DateTimeFieldUpdateOperationsInput, { nullable: true })
  updatedAt?: DateTimeFieldUpdateOperationsInput
}
