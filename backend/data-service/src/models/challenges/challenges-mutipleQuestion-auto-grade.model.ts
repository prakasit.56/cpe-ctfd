import { Field, InputType, ID } from '@nestjs/graphql'

@InputType()
export class MutipleQuestionAutoGrade {
  @Field(() => ID, { nullable: false })
  multipleQuestionId!: string

  @Field(() => ID, { nullable: false })
  multipleChoiceQuestionId!: string

  @Field(() => Boolean, { nullable: false })
  hint!: boolean
}
