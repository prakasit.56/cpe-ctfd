import { Field, ArgsType, ID } from '@nestjs/graphql'
import { ShortAnswerQuestionAutoGrade } from 'src/models/challenges/challenges-shortAnswerQuestion-auto-grade.model'
import { MutipleQuestionAutoGrade } from 'src/models/challenges/challenges-mutipleQuestion-auto-grade.model'
import { FlagQuestionAutoGrade } from 'src/models/challenges/challenges-flagQuestion-auto-grade.model'

@ArgsType()
export class ChallengesAutoGrade {
  @Field(() => ID, { nullable: false })
  challengeId!: string

  @Field(() => Date, { nullable: false })
  startTime!: Date

  @Field(() => Date, { nullable: false })
  endTime!: Date

  @Field(() => [MutipleQuestionAutoGrade], { nullable: true })
  multipleQuestions: Array<MutipleQuestionAutoGrade>

  @Field(() => [FlagQuestionAutoGrade], { nullable: true })
  flagQuestionAutoGrade: Array<FlagQuestionAutoGrade>

  @Field(() => [ShortAnswerQuestionAutoGrade], { nullable: true })
  shortAnswerQuestionAutoGrade: Array<ShortAnswerQuestionAutoGrade>
}
