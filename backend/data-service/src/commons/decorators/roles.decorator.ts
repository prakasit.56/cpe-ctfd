import { SetMetadata } from '@nestjs/common'

export const hasRoles = (role: string[]) => SetMetadata('roles', role)
