import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { GqlExecutionContext } from '@nestjs/graphql'
import * as UserModel from 'src/models/users'
import { Reflector } from '@nestjs/core'

@Injectable()
export class GqlRoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  /**
   * Check the role of a user ,but if user have token, user can bypass it.
   * @param {ExecutionContext} context - ExecutionContext
   * @returns The return value of the canActivate method is a boolean value.
   *     If the value is true, the request is allowed to continue.
   *     If the value is false, the request is rejected.
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const ctx = GqlExecutionContext.create(context)

    const roles_haddler = this.reflector.get<string[]>(
      'roles',
      ctx.getHandler(),
    )
    const roles_class = this.reflector.get<string[]>('roles', ctx.getClass())
    if (!roles_haddler && !roles_class) {
      return false
    }

    const user: UserModel.Users = ctx.getContext().req.user
    if (roles_haddler) return roles_haddler.indexOf(user.user_role.name) > -1
    if (roles_class) return roles_class.indexOf(user.user_role.name) > -1

    return false
  }
}
