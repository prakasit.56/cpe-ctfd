import { PrismaClient } from '@prisma/client'
import { Logger } from '@nestjs/common'
import * as bcrypt from 'bcrypt'

const prisma = new PrismaClient()
const logger = new Logger('Seed Database')

const main = async () => {
  const userRole_admin = await prisma.userRoles.create({
    data: {
      name: 'ADMIN',
    },
  })

  await prisma.users.create({
    data: {
      name: 'admin',
      email: 'admin@gmail.com',
      username: 'admin',
      password: await bcrypt.hash('123qweasdzxc', 10),
      user_role: { connect: { user_role_id: userRole_admin.user_role_id } },
    },
  })

  logger.log(`Create user (admin) done`)

  const userRole_user = await prisma.userRoles.create({
    data: {
      name: 'USER',
    },
  })

  await prisma.users.create({
    data: {
      name: 'user',
      email: 'user@gmail.com',
      username: 'user',
      password: await bcrypt.hash('123qweasdzxc', 10),
      user_role: { connect: { user_role_id: userRole_user.user_role_id } },
    },
  })

  logger.log(`Create user (user) done`)
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
