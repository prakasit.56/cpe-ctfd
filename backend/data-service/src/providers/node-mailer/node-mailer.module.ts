import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { MailerModule } from '@nestjs-modules/mailer'
import { NodeMailerService } from 'src/providers/node-mailer/node-mailer.service'

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        transport: {
          host: config.get<string>('MAIL.HOST'),
          port: config.get<number>('MAIL.PORT'),
          secure: config.get<boolean>('MAIL.SECURE'),
          auth: {
            user: config.get<string>('MAIL.USER'),
            pass: config.get<string>('MAIL.PASS'),
          },
        },
      }),
    }),
    ConfigModule,
  ],
  providers: [NodeMailerService],
  exports: [NodeMailerService],
})
export class NodeMailerModule {}
