import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { MinioModule } from 'nestjs-minio-client'
import { MinioClientService } from './minio.service'

@Module({
  imports: [
    MinioModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          endPoint: process.env.AWS_ENDPOINT,
          accessKey: process.env.AWS_ACCESS_KEY,
          secretKey: process.env.AWS_SECRET_KEY,
          useSSL: configService.get('MINIO.useSSL'),
          region: configService.get('MINIO.REGION'),
        }
      },
    }),
    ConfigModule,
  ],
  providers: [MinioClientService],
  exports: [MinioClientService],
})
export class MinioClientModule {}
