import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as userTutorialsModel from 'src/models/user-tutorials'
import { UserTutorialsService } from 'src/services/userTutorials.service'

@Resolver(() => userTutorialsModel.UserTutorials)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class UserTutorialsResolver {
  private logger: Logger
  constructor(private userTutorialsService: UserTutorialsService) {
    this.logger = new Logger('userTutorialsModel Service')
  }

  /**
   * Get user tutorial records
   */
  @Query(() => [userTutorialsModel.UserTutorials], {
    name: 'getUserTutorials',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getuserTutorialsModel(
    @Args('') args: userTutorialsModel.FindManyUserTutorialsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<userTutorialsModel.UserTutorials[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.userTutorialsService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create user tutorial record
   */
  @Mutation(() => userTutorialsModel.UserTutorials, {
    name: 'createUserTutorials',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async createuserTutorialsModel(
    @Args('') args: userTutorialsModel.CreateOneUserTutorialsArgs,
  ): Promise<userTutorialsModel.UserTutorials | Error> {
    const result = await this.userTutorialsService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update user tutorial record
   */
  @Mutation(() => userTutorialsModel.UserTutorials, {
    name: 'updateUserTutorials',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateuserTutorialsModel(
    @Args('') args: userTutorialsModel.UpdateOneUserTutorialsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<userTutorialsModel.UserTutorials | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.userTutorialsService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete user tutorial record
   */
  @Mutation(() => userTutorialsModel.UserTutorials, {
    name: 'deleteUserTutorials',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteuserTutorialsModel(
    @Args('') args: userTutorialsModel.DeleteOneUserTutorialsArgs,
  ): Promise<userTutorialsModel.UserTutorials | Error> {
    const result = await this.userTutorialsService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
