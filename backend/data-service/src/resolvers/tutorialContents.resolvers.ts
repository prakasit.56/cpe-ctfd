import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as tutorialContentsModel from 'src/models/tutorial-contents'
import { TutorialContentsService } from 'src/services/tutorialContents.service'

@Resolver(() => tutorialContentsModel.TutorialContents)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class TutorialContentsResolver {
  private logger: Logger
  constructor(private tutrialContentsService: TutorialContentsService) {
    this.logger = new Logger('tutorialContents Service')
  }

  /**
   * Get tutorial contents records
   */
  @Query(() => [tutorialContentsModel.TutorialContents], {
    name: 'getTutorialContents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async gettutorialContents(
    @Args('') args: tutorialContentsModel.FindManyTutorialContentsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<tutorialContentsModel.TutorialContents[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.tutrialContentsService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create tutorial contents record
   */
  @Mutation(() => tutorialContentsModel.TutorialContents, {
    name: 'createTutorialContents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createtutorialContents(
    @Args('') args: tutorialContentsModel.CreateOneTutorialContentsArgs,
  ): Promise<tutorialContentsModel.TutorialContents | Error> {
    const result = await this.tutrialContentsService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update tutorial contents record
   */
  @Mutation(() => tutorialContentsModel.TutorialContents, {
    name: 'updateTutorialContents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updatetutorialContents(
    @Args('') args: tutorialContentsModel.UpdateOneTutorialContentsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<tutorialContentsModel.TutorialContents | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.tutrialContentsService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete tutorial contents record
   */
  @Mutation(() => tutorialContentsModel.TutorialContents, {
    name: 'deleteTutorialContents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deletetutorialContents(
    @Args('') args: tutorialContentsModel.DeleteOneTutorialContentsArgs,
  ): Promise<tutorialContentsModel.TutorialContents | Error> {
    const result = await this.tutrialContentsService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
