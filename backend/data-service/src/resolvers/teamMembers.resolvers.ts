import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as TeamMembersModel from 'src/models/team-members'
import { TeamMembersService } from 'src/services/teamMembers.service'

@Resolver(() => TeamMembersModel.TeamMembers)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class TeamMembersResolver {
  private logger: Logger
  constructor(private teamMembersService: TeamMembersService) {
    this.logger = new Logger('TeamMembers Service')
  }

  /**
   * Get Team Member records
   */
  @Query(() => [TeamMembersModel.TeamMembers], {
    name: 'getTeamMembers',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getTeamMembers(
    @Args('') args: TeamMembersModel.FindManyTeamMembersArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<TeamMembersModel.TeamMembers[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.teamMembersService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create Team Member record
   */
  @Mutation(() => TeamMembersModel.TeamMembers, {
    name: 'createTeamMembers',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async createTeamMembers(
    @Args('') args: TeamMembersModel.CreateOneTeamMembersArgs,
  ): Promise<TeamMembersModel.TeamMembers | Error> {
    const result = await this.teamMembersService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update Team Member record
   */
  @Mutation(() => TeamMembersModel.TeamMembers, {
    name: 'updateTeamMembers',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async updateTeamMembers(
    @Args('') args: TeamMembersModel.UpdateOneTeamMembersArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<TeamMembersModel.TeamMembers | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.teamMembersService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete Team Member record
   */
  @Mutation(() => TeamMembersModel.TeamMembers, {
    name: 'deleteTeamMembers',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async deleteTeamMembers(
    @Args('') args: TeamMembersModel.DeleteOneTeamMembersArgs,
  ): Promise<TeamMembersModel.TeamMembers | Error> {
    const result = await this.teamMembersService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
