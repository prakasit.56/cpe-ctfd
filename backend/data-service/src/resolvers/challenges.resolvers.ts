import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import * as UsersModel from 'src/models/users'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as ChallengesModel from 'src/models/challenges'
import { ChallengesService } from 'src/services/challenges.service'

@Resolver(() => ChallengesModel.Challenges)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class ChallengesResolver {
  private logger: Logger
  constructor(private challengesService: ChallengesService) {
    this.logger = new Logger('Challenges Service')
  }

  /**
   * Get challenges records
   */
  @Query(() => [ChallengesModel.Challenges], {
    name: 'getChallenges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getChallenges(
    @Args('') args: ChallengesModel.FindManyChallengesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<ChallengesModel.Challenges[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.challengesService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create challenges record
   */
  @Mutation(() => ChallengesModel.Challenges, {
    name: 'createChallenges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async createChallenges(
    @Args('') args: ChallengesModel.CreateOneChallengesArgs,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<ChallengesModel.Challenges | boolean | Error> {
    const result = await this.challengesService.create(args, user)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update challenges record
   */
  @Mutation(() => ChallengesModel.Challenges, {
    name: 'updateChallenges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateChallenges(
    @Args('') args: ChallengesModel.UpdateOneChallengesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<ChallengesModel.Challenges | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.challengesService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete challenges record
   */
  @Mutation(() => ChallengesModel.Challenges, {
    name: 'deleteChallenges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteChallenges(
    @Args('') args: ChallengesModel.DeleteOneChallengesArgs,
  ): Promise<ChallengesModel.Challenges | Error> {
    const result = await this.challengesService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Auto grade challenges record
   */
  @Mutation(() => Boolean, {
    name: 'autoGrade',
    nullable: false,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async autoGrade(
    @Args('') args: ChallengesModel.ChallengesAutoGrade,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<boolean | Error> {
    const result = await this.challengesService.autoGrade(args, user)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
