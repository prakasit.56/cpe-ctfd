import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as UserBadgesModel from 'src/models/user-badges'
import { UserBadgesService } from 'src/services/userBadges.service'

@Resolver(() => UserBadgesModel.UserBadges)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class UserBadgesResolver {
  private logger: Logger
  constructor(private userBadgesService: UserBadgesService) {
    this.logger = new Logger('UserBadges Service')
  }

  /**
   * Get user badge records
   */
  @Query(() => [UserBadgesModel.UserBadges], {
    name: 'getUserBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getUserBadges(
    @Args('') args: UserBadgesModel.FindManyUserBadgesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<UserBadgesModel.UserBadges[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.userBadgesService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create user badge record
   */
  @Mutation(() => UserBadgesModel.UserBadges, {
    name: 'createUserBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createUserBadges(
    @Args('') args: UserBadgesModel.CreateOneUserBadgesArgs,
  ): Promise<UserBadgesModel.UserBadges | Error> {
    const result = await this.userBadgesService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update user badge record
   */
  @Mutation(() => UserBadgesModel.UserBadges, {
    name: 'updateUserBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateUserBadges(
    @Args('') args: UserBadgesModel.UpdateOneUserBadgesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<UserBadgesModel.UserBadges | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.userBadgesService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete user badge record
   */
  @Mutation(() => UserBadgesModel.UserBadges, {
    name: 'deleteUserBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteUserBadges(
    @Args('') args: UserBadgesModel.DeleteOneUserBadgesArgs,
  ): Promise<UserBadgesModel.UserBadges | Error> {
    const result = await this.userBadgesService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
