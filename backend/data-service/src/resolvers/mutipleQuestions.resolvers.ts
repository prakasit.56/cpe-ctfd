import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as MutipleQuestionsModel from 'src/models/multiple-questions'
import { MultipleQuestionsService } from 'src/services/multipleQuestions.service'

@Resolver(() => MutipleQuestionsModel.MultipleQuestions)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class MutipleQuestionsResolver {
  private logger: Logger
  constructor(private mutipleQuestionsService: MultipleQuestionsService) {
    this.logger = new Logger('MutipleQuestions Service')
  }

  /**
   * Get mutipleQuestions records
   */
  @Query(() => [MutipleQuestionsModel.MultipleQuestions], {
    name: 'getMutipleQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getMutipleQuestions(
    @Args('') args: MutipleQuestionsModel.FindManyMultipleQuestionsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<MutipleQuestionsModel.MultipleQuestions[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.mutipleQuestionsService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create mutipleQuestions record
   */
  @Mutation(() => MutipleQuestionsModel.MultipleQuestions, {
    name: 'createMutipleQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createMutipleQuestions(
    @Args('') args: MutipleQuestionsModel.CreateOneMultipleQuestionsArgs,
  ): Promise<MutipleQuestionsModel.MultipleQuestions | Error> {
    const result = await this.mutipleQuestionsService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update mutipleQuestions record
   */
  @Mutation(() => MutipleQuestionsModel.MultipleQuestions, {
    name: 'updateMutipleQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateMutipleQuestions(
    @Args('') args: MutipleQuestionsModel.UpdateOneMultipleQuestionsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<MutipleQuestionsModel.MultipleQuestions | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.mutipleQuestionsService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete mutipleQuestions record
   */
  @Mutation(() => MutipleQuestionsModel.MultipleQuestions, {
    name: 'deleteMutipleQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteMutipleQuestions(
    @Args('') args: MutipleQuestionsModel.DeleteOneMultipleQuestionsArgs,
  ): Promise<MutipleQuestionsModel.MultipleQuestions | Error> {
    const result = await this.mutipleQuestionsService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
