import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as UsersModel from 'src/models/users'
import * as UserTokensModel from 'src/models/user-tokens'
import { UsersService } from 'src/services/users.service'

@Resolver(() => UsersModel.Users)
export class UsersResolver {
  private logger: Logger
  constructor(private usersService: UsersService) {
    this.logger = new Logger('User Service')
  }

  /**
   * Get User records
   */
  @Query(() => [UsersModel.UsersWithoutPassword], {
    name: 'getUsers',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  @UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
  async getUsers(
    @Args('') args: UsersModel.FindManyUsersArgs,
    @Info() info: GraphQLResolveInfo,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<UsersModel.UsersWithoutPassword[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.usersService.get(args, select, user)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create User record or register
   */
  @Mutation(() => UsersModel.UsersWithoutPassword, {
    name: 'createUser',
    nullable: true,
  })
  async createUser(
    @Args('') args: UsersModel.CreateOneUsersArgs,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    // Replace role
    args.data.user_role.connect.user_role_id = ''

    const result = await this.usersService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update User record
   */
  @Mutation(() => UsersModel.UsersWithoutPassword, {
    name: 'updateUser',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  @UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
  async updateUser(
    @Args('') args: UsersModel.UpdateOneUsersArgs,
    @Info() info: GraphQLResolveInfo,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.usersService.update(args, select, user)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete User record
   */
  @Mutation(() => UsersModel.UsersWithoutPassword, {
    name: 'deleteUser',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  @UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
  async deleteUser(
    @Args('') args: UsersModel.DeleteOneUsersArgs,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    const result = await this.usersService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Login
   */
  @Mutation(() => UsersModel.UsersLoginOutput, {
    name: 'login',
    nullable: true,
  })
  async login(
    @Args('data') data: UsersModel.UsersLoginInput,
  ): Promise<UsersModel.UsersLoginOutput | Error> {
    const result = await this.usersService.login(data)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Logout
   */
  @Mutation(() => Boolean, {
    name: 'logout',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  @UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
  async logout(
    @Args('data') data: UsersModel.UsersLogoutInput,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<boolean | Error> {
    data.user_id = user.user_id
    const result = await this.usersService.logout(data)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
  /**
   * Refresh Token
   */
  @Mutation(() => UserTokensModel.UserRefreshTokensOutput, {
    name: 'refreshToken',
    nullable: true,
  })
  async refreshToken(
    @Args('data') data: UserTokensModel.UserRefreshTokensInput,
  ): Promise<UserTokensModel.UserRefreshTokensOutput | Error> {
    const result = await this.usersService.refreshToken(data)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  @Mutation(() => UsersModel.UsersWithoutPassword, {
    name: 'updateImageUser',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  @UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
  async updateImageCompany(
    @Args('') args: UsersModel.UpdateOneUserImageArgs,
    @Info() info: GraphQLResolveInfo,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<UsersModel.UsersWithoutPassword | Error> {
    /**
     * Update Image user
     */
    const select = new PrismaSelect(info).value
    const result = await this.usersService.updateImage(args, select, user)
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
