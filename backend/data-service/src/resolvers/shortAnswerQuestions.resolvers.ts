import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as ShortAnswerQuestionsModel from 'src/models/short-answer-questions'
import { ShortAnswerQuestionsService } from 'src/services/shortAnswerQuestions.service'

@Resolver(() => ShortAnswerQuestionsModel.ShortAnswerQuestions)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class ShortAnswerQuestionsResolver {
  private logger: Logger
  constructor(
    private shortAnswerQuestionsService: ShortAnswerQuestionsService,
  ) {
    this.logger = new Logger('ShortAnswerQuestions Service')
  }

  /**
   * Get shortAnswerQuestions records
   */
  @Query(() => [ShortAnswerQuestionsModel.ShortAnswerQuestions], {
    name: 'getShortAnswerQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getShortAnswerQuestions(
    @Args('') args: ShortAnswerQuestionsModel.FindManyShortAnswerQuestionsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.shortAnswerQuestionsService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create shortAnswerQuestions record
   */
  @Mutation(() => ShortAnswerQuestionsModel.ShortAnswerQuestions, {
    name: 'createShortAnswerQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createShortAnswerQuestions(
    @Args('') args: ShortAnswerQuestionsModel.CreateOneShortAnswerQuestionsArgs,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions | Error> {
    const result = await this.shortAnswerQuestionsService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update shortAnswerQuestions record
   */
  @Mutation(() => ShortAnswerQuestionsModel.ShortAnswerQuestions, {
    name: 'updateShortAnswerQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateShortAnswerQuestions(
    @Args('') args: ShortAnswerQuestionsModel.UpdateOneShortAnswerQuestionsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.shortAnswerQuestionsService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete shortAnswerQuestions record
   */
  @Mutation(() => ShortAnswerQuestionsModel.ShortAnswerQuestions, {
    name: 'deleteShortAnswerQuestions',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteShortAnswerQuestions(
    @Args('') args: ShortAnswerQuestionsModel.DeleteOneShortAnswerQuestionsArgs,
  ): Promise<ShortAnswerQuestionsModel.ShortAnswerQuestions | Error> {
    const result = await this.shortAnswerQuestionsService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
