import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import * as UsersModel from 'src/models/users'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as tutorialChapterDatasModel from 'src/models/tutorial-chapter-datas'
import { TutorialChapterDatasService } from 'src/services/tutorialChapterDatas.service'

@Resolver(() => tutorialChapterDatasModel.TutorialChapterDatas)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class TutorialChapterDatasResolver {
  private logger: Logger
  constructor(private tutorialChaperDatasService: TutorialChapterDatasService) {
    this.logger = new Logger('tutorialChapterDatas Service')
  }

  /**
   * Get tutorial contents records
   */
  @Query(() => [tutorialChapterDatasModel.TutorialChapterDatas], {
    name: 'getTutorialChapterDatas',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async gettutorialChapterDatas(
    @Args('') args: tutorialChapterDatasModel.FindManyTutorialChapterDatasArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<tutorialChapterDatasModel.TutorialChapterDatas[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.tutorialChaperDatasService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create tutorial contents record
   */
  @Mutation(() => tutorialChapterDatasModel.TutorialChapterDatas, {
    name: 'createTutorialChapterDatas',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createtutorialChapterDatas(
    @Args('') args: tutorialChapterDatasModel.CreateOneTutorialChapterDatasArgs,
  ): Promise<tutorialChapterDatasModel.TutorialChapterDatas | Error> {
    const result = await this.tutorialChaperDatasService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update tutorial contents record
   */
  @Mutation(() => tutorialChapterDatasModel.TutorialChapterDatas, {
    name: 'updateTutorialChapterDatas',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updatetutorialChapterDatas(
    @Args('') args: tutorialChapterDatasModel.UpdateOneTutorialChapterDatasArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<tutorialChapterDatasModel.TutorialChapterDatas | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.tutorialChaperDatasService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete tutorial contents record
   */
  @Mutation(() => tutorialChapterDatasModel.TutorialChapterDatas, {
    name: 'deleteTutorialChapterDatas',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deletetutorialChapterDatas(
    @Args('') args: tutorialChapterDatasModel.DeleteOneTutorialChapterDatasArgs,
  ): Promise<tutorialChapterDatasModel.TutorialChapterDatas | Error> {
    const result = await this.tutorialChaperDatasService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Auto grade tutorial record
   */
  @Mutation(() => Boolean, {
    name: 'autoGradeTutorialChapter',
    nullable: false,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async autoGrade(
    @Args('') args: tutorialChapterDatasModel.TutorialAutoGrade,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<boolean | Error> {
    const result = await this.tutorialChaperDatasService.autoGrade(args, user)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
