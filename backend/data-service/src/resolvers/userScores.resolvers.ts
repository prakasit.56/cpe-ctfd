import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as UserScoresModel from 'src/models/user-scores'
import { UserScoresService } from 'src/services/userScores.service'

@Resolver(() => UserScoresModel.UserScores)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class UserScoresResolver {
  private logger: Logger
  constructor(private userScoresService: UserScoresService) {
    this.logger = new Logger('UserScores Service')
  }

  /**
   * Get user scores records
   */
  @Query(() => [UserScoresModel.UserScores], {
    name: 'getUserScores',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getUserScores(
    @Args('') args: UserScoresModel.FindManyUserScoresArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<UserScoresModel.UserScores[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.userScoresService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create user scores record
   */
  @Mutation(() => UserScoresModel.UserScores, {
    name: 'createUserScores',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async createUserScores(
    @Args('') args: UserScoresModel.CreateOneUserScoresArgs,
  ): Promise<UserScoresModel.UserScores | Error> {
    const result = await this.userScoresService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update user scores record
   */
  @Mutation(() => UserScoresModel.UserScores, {
    name: 'updateUserScores',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateUserScores(
    @Args('') args: UserScoresModel.UpdateOneUserScoresArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<UserScoresModel.UserScores | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.userScoresService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete user scores record
   */
  @Mutation(() => UserScoresModel.UserScores, {
    name: 'deleteUserScores',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async deleteUserScores(
    @Args('') args: UserScoresModel.DeleteOneUserScoresArgs,
  ): Promise<UserScoresModel.UserScores | Error> {
    const result = await this.userScoresService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * GroupBy user scores record
   */
  @Query(() => [UserScoresModel.UserScoresGroupByForTotalScore], {
    name: 'groupByUserScores',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async groupByUserScores(
    @Args('') args: UserScoresModel.UserScoresGroupByLeaderBoardArgs,
  ): Promise<UserScoresModel.UserScoresGroupByForTotalScore[] | Error> {
    const result = await this.userScoresService.groupBy(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
