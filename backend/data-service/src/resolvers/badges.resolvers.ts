import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as BadgesModel from 'src/models/badges'
import { BadgesService } from 'src/services/badges.service'

@Resolver(() => BadgesModel.Badges)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class BadgesResolver {
  private logger: Logger
  constructor(private badgesService: BadgesService) {
    this.logger = new Logger('Badges Service')
  }

  /**
   * Get Badge records
   */
  @Query(() => [BadgesModel.Badges], {
    name: 'getBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getBadges(
    @Args('') args: BadgesModel.FindManyBadgesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<BadgesModel.Badges[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.badgesService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create Badge record
   */
  @Mutation(() => BadgesModel.Badges, {
    name: 'createBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createBadges(
    @Args('') args: BadgesModel.CreateOneBadgesArgs,
  ): Promise<BadgesModel.Badges | Error> {
    const result = await this.badgesService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update Badge record
   */
  @Mutation(() => BadgesModel.Badges, {
    name: 'updateBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateBadges(
    @Args('') args: BadgesModel.UpdateOneBadgesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<BadgesModel.Badges | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.badgesService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete Badge record
   */
  @Mutation(() => BadgesModel.Badges, {
    name: 'deleteBadges',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteBadges(
    @Args('') args: BadgesModel.DeleteOneBadgesArgs,
  ): Promise<BadgesModel.Badges | Error> {
    const result = await this.badgesService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
