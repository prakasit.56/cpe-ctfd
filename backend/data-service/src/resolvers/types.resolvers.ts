import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as TypesModel from 'src/models/types'
import { TypesService } from 'src/services/types.service'

@Resolver(() => TypesModel.Types)
@hasRoles([UserRoleModel.Role.ADMIN])
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class TypesResolver {
  private logger: Logger
  constructor(private typesService: TypesService) {
    this.logger = new Logger('Types Service')
  }

  /**
   * Get type records
   */
  @Query(() => [TypesModel.Types], {
    name: 'getTypes',
    nullable: true,
  })
  // @UseGuards(GqlAuthGuard)
  async getTypes(
    @Args('') args: TypesModel.FindManyTypesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<TypesModel.Types[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.typesService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create type record
   */
  @Mutation(() => TypesModel.Types, {
    name: 'createTypes',
    nullable: true,
  })
  async createTypes(
    @Args('') args: TypesModel.CreateOneTypesArgs,
  ): Promise<TypesModel.Types | Error> {
    const result = await this.typesService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update type record
   */
  @Mutation(() => TypesModel.Types, {
    name: 'updateTypes',
    nullable: true,
  })
  async updateTypes(
    @Args('') args: TypesModel.UpdateOneTypesArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<TypesModel.Types | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.typesService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete type record
   */
  @Mutation(() => TypesModel.Types, {
    name: 'deleteTypes',
    nullable: true,
  })
  // @UseGuards(GqlAuthGuard)
  async deleteTypes(
    @Args('') args: TypesModel.DeleteOneTypesArgs,
  ): Promise<TypesModel.Types | Error> {
    const result = await this.typesService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
