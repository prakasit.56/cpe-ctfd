import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as TeamsModel from 'src/models/teams'
import * as UsersModel from 'src/models/users'
import { TeamsService } from 'src/services/teams.service'

@Resolver(() => TeamsModel.Teams)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class TeamsResolver {
  private logger: Logger
  constructor(private teamService: TeamsService) {
    this.logger = new Logger('Teamss Service')
  }

  /**
   * Get Teams records
   */
  @Query(() => [TeamsModel.Teams], {
    name: 'getTeams',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getTeams(
    @Args('') args: TeamsModel.FindManyTeamsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<TeamsModel.Teams[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.teamService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create Teams record
   */
  @Mutation(() => TeamsModel.Teams, {
    name: 'createTeams',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async createTeams(
    @Args('') args: TeamsModel.CreateOneTeamsArgs,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<TeamsModel.Teams | Error> {
    args.data.teamMembers.createMany.data[0].user_id = user.user_id

    const result = await this.teamService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update Teams record
   */
  @Mutation(() => TeamsModel.Teams, {
    name: 'updateTeams',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async updateTeams(
    @Args('') args: TeamsModel.UpdateOneTeamsArgs,
    @Info() info: GraphQLResolveInfo,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<TeamsModel.Teams | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.teamService.update(args, select, user)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete Teams record
   */
  @Mutation(() => TeamsModel.Teams, {
    name: 'deleteTeams',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async deleteTeams(
    @Args('') args: TeamsModel.DeleteOneTeamsArgs,
  ): Promise<TeamsModel.Teams | Error> {
    const result = await this.teamService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  @Mutation(() => TeamsModel.Teams, {
    name: 'updateImageTeam',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  @UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
  async updateImageCompany(
    @Args('') args: TeamsModel.UpdateOneTeamImageArgs,
    @Info() info: GraphQLResolveInfo,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<TeamsModel.Teams | Error> {
    /**
     * Update Image user
     */
    const select = new PrismaSelect(info).value
    const result = await this.teamService.updateImage(args, select, user)
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
