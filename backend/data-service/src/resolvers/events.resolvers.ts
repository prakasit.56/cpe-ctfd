import { UseGuards, Logger } from '@nestjs/common'
import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql'
import { auth, role } from 'src/commons/guards'
import { hasRoles } from 'src/commons/decorators'
import * as UserRoleModel from 'src/models/user-roles'
import { PrismaSelect } from '@paljs/plugins'
import { GraphQLResolveInfo } from 'graphql'
import * as EventsModel from 'src/models/events'
import * as UsersModel from 'src/models/users'
import { EventsService } from 'src/services/events.service'

@Resolver(() => EventsModel.Events)
@UseGuards(auth.GqlAuthAccessGuard, role.GqlRoleGuard)
export class EventsResolver {
  private logger: Logger
  constructor(private eventsService: EventsService) {
    this.logger = new Logger('Events Service')
  }

  /**
   * Get Event records
   */
  @Query(() => [EventsModel.Events], {
    name: 'getEvents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async getEvents(
    @Args('') args: EventsModel.FindManyEventsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<EventsModel.Events[] | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.eventsService.get(args, select)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Create Event record
   */
  @Mutation(() => EventsModel.Events, {
    name: 'createEvents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async createEvents(
    @Args('') args: EventsModel.CreateOneEventsArgs,
  ): Promise<EventsModel.Events | Error> {
    const result = await this.eventsService.create(args)

    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Update Event record
   */
  @Mutation(() => EventsModel.Events, {
    name: 'updateEvents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async updateEvents(
    @Args('') args: EventsModel.UpdateOneEventsArgs,
    @Info() info: GraphQLResolveInfo,
  ): Promise<EventsModel.Events | Error> {
    const select = new PrismaSelect(info).value
    const result = await this.eventsService.update(args, select)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Delete Event record
   */
  @Mutation(() => EventsModel.Events, {
    name: 'deleteEvents',
    nullable: true,
  })
  @hasRoles([UserRoleModel.Role.ADMIN])
  async deleteEvents(
    @Args('') args: EventsModel.DeleteOneEventsArgs,
  ): Promise<EventsModel.Events | Error> {
    const result = await this.eventsService.delete(args)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }

  /**
   * Auto grade event record
   */
  @Mutation(() => Boolean, {
    name: 'autoGradeEvent',
    nullable: false,
  })
  @hasRoles([UserRoleModel.Role.ADMIN, UserRoleModel.Role.USER])
  async autoGrade(
    @Args('') args: EventsModel.EventAutoGrade,
    @auth.CurrentUser() user: UsersModel.Users,
  ): Promise<boolean | Error> {
    const result = await this.eventsService.autoGrade(args, user)
    // Error
    if (result instanceof Error) this.logger.error(`${result}`)

    return result
  }
}
