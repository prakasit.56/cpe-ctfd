#!/usr/bin/env sh
set -eu

envsubst '${DOMAIN}' < nginx.conf.template > nginx.conf

nginx -g "daemon off;"