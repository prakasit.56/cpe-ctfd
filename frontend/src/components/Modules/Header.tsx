import { FC } from "react";

import * as NavBar from "components/Navbar";
import { general } from 'utils'


interface IHeaderProps{}

const Header: FC<IHeaderProps> = () => {

  // Create logic for show or hide header in here
  return (
    <>
      {
        general.auth.isAuthorized() ? <NavBar.NavBarAfterLogin/> : <NavBar.NavBarBeforeLogin/>
      }
    </>
  );
};

export default Header;
