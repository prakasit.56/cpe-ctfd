import { FC } from "react";

interface IFooterProps{}

const Footer: FC<IFooterProps> = () => {

  // Create logic for show or hide footer in here
  return (
    <>
      <div className="w-full h-20">
          this is Footer
      </div>
    </>
  );
};

export default Footer;
