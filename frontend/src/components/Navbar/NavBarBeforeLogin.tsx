import logo from "assets/ctf-logo.svg";

const NavBarBeforeLogin = () => {
  return (
    <>
      <div className="flex h-20 items-center justify-between bg-[#0A0527] px-8 shadow-md">
        <div className="flex items-center justify-center">
          <button
              onClick={() =>  window.location.href='/'}
            >
            <img src={logo} alt="ctfd-32" className="h-full w-20" />
          </button>
          <button
              onClick={() =>  window.location.href='/'}
            >
            <h6 className="ml-24 text-[#0FB1D9] duration-300 ease-in-out hover:text-white font-Roboto text-base">
              Home
            </h6>
          </button>
        </div>
        <div className="flex gap-6">
          <div className="sticky">
            <button
              onClick={() =>  window.location.href='/sign-up'}
            >
              <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline font-Roboto text-base">
                Sign up
              </h6>
            </button>
          </div>
          <div className="sticky">
            <button
              onClick={() =>  window.location.href='/sign-in'}
            >
              <h6 className="text-[#0FB1D9] duration-300 ease-in-out hover:text-white inline-flex font-Roboto text-base">
                Sign in
              </h6>
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default NavBarBeforeLogin;
