export const CreateShortDataFieldForm = () => [
    {
      label: 'Challenge Name',
      name: 'name',
      type: 'text',
      placeholder: 'Challenge Name',
    },
    {
      label: 'Question',
      name: 'question',
      type: 'text',
      placeholder: 'Question',
    },
    {
      label: 'Description',
      name: 'description',
      type: 'text',
      placeholder: 'Description',
    },
    {
      label: 'Hint',
      name: 'hint',
      type: 'text',
      placeholder: 'Hint',
    },
    {
      label: 'Answer',
      name: 'answer',
      type: 'answer',
      placeholder: 'Answer',
    },
  ]

  export const CreateFlagDataFieldForm = () => [
    {
      label: 'Challenge Name',
      name: 'name',
      type: 'text',
      placeholder: 'Challenge Name',
    },
    {
      label: 'Question',
      name: 'question',
      type: 'text',
      placeholder: 'Question',
    },
    {
      label: 'Description',
      name: 'description',
      type: 'text',
      placeholder: 'Description',
    },
    {
      label: 'Video Link',
      name: 'video_link',
      type: 'text',
      placeholder: 'Video Link',
    },
    {
      label: 'Hint',
      name: 'hint',
      type: 'text',
      placeholder: 'Hint',
    },
    {
      label: 'Answer',
      name: 'answer',
      type: 'text',
      placeholder: 'Answer',
    },
  ]

  export const CreateMultipleDataFieldForm = () => [
    {
      label: 'Challenge Name',
      name: 'challenge_name',
      type: 'text',
      placeholder: 'Challenge Name',
    },
    {
        label: 'Question',
        name: 'question',
        type: 'text',
        placeholder: 'Question',
      },
      {
        label: 'Hint',
        name: 'hint',
        type: 'text',
        placeholder: 'Hint',
      },
      {
        label: 'Choice 1',
        name: 'choice_1',
        type: 'text',
        placeholder: 'Choice 1',
      },
      {
        label: 'Choice 2',
        name: 'choice_2',
        type: 'text',
        placeholder: 'Choice 2',
      },
      {
        label: 'Choice 3',
        name: 'choice_3',
        type: 'text',
        placeholder: 'Choice 3',
      },
      {
        label: 'Choice 4',
        name: 'choice_4',
        type: 'text',
        placeholder: 'Choice 4',
      },
  ]