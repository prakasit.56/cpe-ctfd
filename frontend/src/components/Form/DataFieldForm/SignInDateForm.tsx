export const signInDataFieldFormUsername = () => [
    {
      label: 'Username',
      name: 'username',
      type: 'text',
      placeholder: 'username',
    },
  ]

export const signInDataFieldFormPassword = () => [
  {
    label: 'Password',
    name: 'password',
    type: 'password',
    placeholder: 'password',
  },
]