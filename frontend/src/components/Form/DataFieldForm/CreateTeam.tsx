export const CreateShortDataFieldForm = () => [
    {
      label: 'Team Name',
      name: 'name',
      type: 'text',
      placeholder: 'Team Name',
    },
    {
      label: 'Country',
      name: 'country_code',
      type: 'text',
      placeholder: 'Country',
    },
    {
      label: 'Github',
      name: 'github_link',
      type: 'string',
      placeholder: 'Description',
    },
    {
      label: 'Twitter',
      name: 'twitter_link',
      type: 'string',
      placeholder: 'Hint',
    },
    {
      label: 'Facebook',
      name: 'facebook_link',
      type: 'string',
      placeholder: 'Answer',
    },
  ]