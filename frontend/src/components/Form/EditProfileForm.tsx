import { Formik, Form } from 'formik'
import { FC, InputHTMLAttributes, useState, useMemo } from 'react'
import { RiAccountCircleFill } from "react-icons/ri";
import Select from "react-select";
import countryList from "react-select-country-list";
import { Field, ErrorMessage } from "formik";
import * as validateSchemaForm from "constants/validateSchemaForm";
import RenderError from "components/Form/RenderError";

interface IFormProps extends InputHTMLAttributes<HTMLInputElement> {
  touched: any;
  errors: any;
  label: string;
  name: string;
  placeholder: string;
  type: string;
  min?: number;
}

const customStyles = {
  valueContainer: (provided, state) => ({
    ...provided,
    borderradius: "8px",
    fontsize: "small",
    padding: "0.625 rem",
  }),

  control: (styles, state) => ({
    ...styles,
    backgroundColor: "#1A2332",
    boxShadow: state.isFocused ? "#1A2332" : "#1A2332",
    borderColor: state.isFocused ? "#1A2332" : "#1A2332",
    "&:hover": {
      borderColor: state.isFocused ? "white" : "white",
    },
  }),

  option: (provided, state) => ({
    ...provided,
    //   borderBottom: '1px dotted pink',
    //   color: state.isSelected ? 'red' : 'blue',
    padding: 20,
    color: state.isSelected ? "white" : "black",
  }),

  singleValue: (provided) => ({
    ...provided,
    color: "white",
    padding: "0.625rem",
  }),

  placeholder: (defaultStyles) => {
    return {
      ...defaultStyles,
      color: "grey",
    };
  },

  input: (base, state) => ({
    ...base,
    color: "white",
    marginTop: "-2px",
  }),
};

const DefaultForm: FC<IFormProps> = (props) => {
  const { touched, errors, label, name, placeholder, type, min } = props;

  const [value, setValue] = useState('')
  const options = useMemo(() => countryList().getData(), [])

  const changeHandler = value => {
      setValue(value)
  }

  return (
    <div className="flex flex-col gap-1">
      <label
        className={`text-s text-left ${
          touched && errors ? "text-red-400" : "text-white"
        }`}
        htmlFor={name}
      >
        {label}
      </label>
      <Field
        id={name}
        name={name}
        className={`${
          touched && errors
            ? "border-red-400 text-red-700 focus:bg-red-200"
            : "border-gray-400 text-white focus:bg-gray-200"
        } h-12 rounded-xl bg-primary px-4 py-2 focus:bg-primary`}
        type={type}
        min={min}
        aria-label={placeholder}
        placeholder={placeholder}
      />
      <ErrorMessage name={name} render={RenderError} />
    </div>
  );
};

DefaultForm.defaultProps = {
  min: 0,
};

export default DefaultForm;
