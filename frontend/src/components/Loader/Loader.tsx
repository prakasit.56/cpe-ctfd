function Loader() {
    return (
    <div className="flex justify-center items-center cursor-default bg-black opacity-70 fixed inset-0 w-full h-full">
      <div className="spinner-border animate-spin inline-block w-12 h-12 border-4 rounded-full text-white" role="status">
        <span className="visually-hidden">Loading...</span>
      </div>
    </div>
  )
}

export default Loader;