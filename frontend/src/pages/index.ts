import * as Public from 'pages/PublicRoutes'
import * as Private from 'pages/PrivateRoutes'
import * as Test from 'pages/Testfile'

export {
    Public,
    Private,
    Test
}
