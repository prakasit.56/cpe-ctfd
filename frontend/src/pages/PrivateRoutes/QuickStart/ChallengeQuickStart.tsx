import { FC } from 'react';
import MainChallenge from 'pages/PrivateRoutes/Challenge/MainChallenge';

const ChallengeQuickStart: FC = () => {
    return (
        <div className='cursor-default bg-black opacity-70 fixed inset-0 w-full h-full'>
            <div className='py-20'>
                <div>Click here to start Event Mode. You can try to complete challenge in this feature.</div>
                <br/>
                <div className='flex gap-5 justify-center'>
                    <a href="/leaderboard-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:bg-white hover:text-[#344663]'>Previous</button></a>
                    <a href="/profile-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:bg-white hover:text-[#344663]'>Next</button></a>
                    <a href="/main-events"><button className='text-base w-32 h-12 bg-[#344663] hover:bg-white hover:text-[#344663]'>Skip</button></a>
                </div>            
                <div className='opacity-20'>
                    <MainChallenge />
                </div>
            </div>
            
        </div>
        )
}
export default ChallengeQuickStart
