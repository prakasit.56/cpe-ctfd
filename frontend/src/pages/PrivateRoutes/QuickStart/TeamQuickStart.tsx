import { FC } from 'react';
import TeamProfile from "pages/PrivateRoutes/Team/TeamProfile";

const TeamQuickStart: FC = () => {
    return (
        <div className='cursor-default bg-black opacity-70 fixed inset-0 w-full h-full justify-center'>
            <div className='py-20'>
                <div className='justify-center'>Click here to find the team or create your team with your friends.</div>
                <br/>
                <div className='flex gap-5 justify-center'>
                    <a href="/profile-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Previous</button></a>
                    <a href="/main-events"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Next</button></a>
                    <a href="/main-events"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Skip</button></a>
                </div>
                <div className='opacity-20 grid place-items-center '>
                <TeamProfile />
            </div>
            </div>
            
        </div>
        )
}
export default TeamQuickStart
