import React, { FC } from 'react';
import Home from 'pages/PublicRoutes/Home/HomePage'

const HomeQuickStart: FC = () => {
    return (
        <div className='cursor-default bg-black opacity-70 fixed inset-0 w-full h-full'>
            <div className='py-20'>
                <div className='justify-center'>Welcome to the CPE32 Playground let’s we show you what we can do for you.</div>
                <br/>
                <div className='flex gap-5 justify-center'>
                    <a href="/tutorial-quickstart"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Next</button></a>
                    <a href="/main-events"><button className='text-base w-32 h-12 bg-[#344663] hover:text-[#344663] hover:bg-white'>Skip</button></a>
                </div>
                <div className='opacity-20'>
                    <Home/>
                </div>
            </div>
            
            
        </div>
        )
}
export default HomeQuickStart
