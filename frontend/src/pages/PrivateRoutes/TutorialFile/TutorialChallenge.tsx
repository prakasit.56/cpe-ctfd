import React, { FC } from 'react'

const TutorialChallenge: FC = () => {
    const [showFirstModal, setShowFirstModal] = React.useState(false);
    const [showSecondModal, setShowSecondModal] = React.useState(false);
    return (
        <div className='w-7/12 bg-[#1A2332] text-lg px-10 py-8 rounded-xl'>
            <div>
                <div className='text-3xl'>Tutorial Name</div>
            </div>
            <br/><br/><br/><br/>
            <div>
                <div>
                    <div className='text-left'>
                        <div className=' text-2xl text-[#0FB1D9]'>Find Flag</div>
                        <br/>
                        <h6>Follow the steps in this task. What is the flag text shown on the website of the machine 
                        you started on this task? A flag is just a piece of text that’s used to verify
                        you’ve performed a certain action. In security challenges, 
                        users are asked to find flags to prove that they’ve successfully hacked a machine</h6>
                        <br/>
                    </div>
                        
                    <div>
                        <input type="text" placeholder='Submit your answer here' className='bg-[#455D84]/[.5] h-28'/>
                    </div>
                    <br/>
                    <div className='h-16 flex bg-[#455D84]/[.5] rounded-md justify-between items-center px-2'>
                        <div className='flex gap-2'>
                        <button className='bg-[#0062B9] hover:text-[#0062B9] hover:bg-white w-36 h-12'>Previous</button>
                        <button
                            className="w-36 h-12 bg-[#0062B9] hover:text-[#0062B9] hover:bg-white"
                            type="button"
                            onClick={() => setShowFirstModal(true)}
                        >
                            Submit
                            {/* <i className='mr-2'>
                                {ButtonIcon}
                            </i> */}
                        </button>
                        {showFirstModal ? (
                            <>
                            <div
                                className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                            >
                                <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                    <h3 className="text-3xl font-semibold text-black">
                                        Confirm
                                    </h3>
                                    <button
                                        className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                        onClick={() => setShowFirstModal(false)}
                                    >
                                        ×
                                    </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                    <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                        Are you sure you want to sent your answer?
                                    </p>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                    <button
                                        className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                        type="button"
                                        onClick={() => setShowFirstModal(false)}
                                    >
                                        Back
                                    </button>
                                    <button
                                        className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                        type="button"
                                        onClick={() => setShowSecondModal(true)}
                                        // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                                    >
                                        Continue
                                        {/* <i className='mr-2'>
                                            {ButtonIcon}
                                        </i> */}
                                    </button>
                                    {showSecondModal ? (
                                        <>
                                        <div
                                            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                                        >
                                            <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                            {/*content*/}
                                            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                                {/*header*/}
                                                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                                <h3 className="text-3xl font-semibold text-black">
                                                    Thank you
                                                </h3>
                                                <button
                                                    className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                                    // onClick={() => setShowSecondModal(false)}
                                                    onClick={() => [setShowSecondModal(false), setShowFirstModal(false)]}
                                                >
                                                    ×
                                                </button>
                                                </div>
                                                {/*body*/}
                                                <div className="relative p-6 flex-auto">
                                                <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                                    Thank you! Your answer has been send.
                                                </p>
                                                </div>
                                                {/*footer*/}
                                                <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                                <button
                                                    className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                                    type="button"
                                                    // onClick={() => setShowSecondModal(false)}
                                                    onClick={() => [setShowSecondModal(false), setShowFirstModal(false)]}
                                                >
                                                    Continue
                                                </button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                                        </>
                                    ) : null}
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                            </>
                        ) : null}                        
                        </div>
                        <a href="#" className='text-yellow-400 px-5'>Take Hint <div className='display: inline'>(-15 points)</div></a>
                    </div>
                </div>
            </div>
        </div>
        )
}
export default TutorialChallenge
