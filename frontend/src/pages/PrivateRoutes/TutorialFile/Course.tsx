import React, { FC } from 'react'
import { FaArrowAltCircleLeft, FaArrowAltCircleRight } from "react-icons/fa";

const Course: FC = () => {
    return (
        <div className='container w-8/12 justify-center'>
            <div className='text-3xl text-bold'>Course Name</div>
            <br/>
            {/* <div
                style={{
                backgroundSize: "cover",
                backgroundRepeat: "no-repat",
                backgroundBlendMode: "multiply",
                backgroundColor: "white"
                }}
                className={`group relative flex h-96
                            w-full items-center justify-center shadow-md mb-1`}
            >
                
                <h6 className="text-xl font-semibold text-black">Video</h6>
            </div> */}
            <div className='flex bg-white w-full h-96 items-center justify-center text-black'> 
                VDO
            </div>
            <br/>
            <div className='text-xl text-yellow-600 grid gap-y-2'>
                <div className='h-14 w-8/10 bg-[#1A2332] rounded mb-1 py-3'>
                    <a href="#">Chapter 1 : Introduction Cybersecurity Fundamental</a>
                </div>
                <div className='h-14 w-8/10 bg-[#1A2332] rounded mb-1 py-3'>
                    <a href="#">Chapter 2 : Introduction Cybersecurity Fundamental</a>
                </div>
                <div className='h-14 w-8/10 bg-[#1A2332] rounded mb-1 py-3'>
                    <a href="#">Chapter 3 : Introduction Cybersecurity Fundamental</a>
                </div>
            </div>
            <br/>
            <div className='flex justify-end gap-x-4'>
                <button className='button flex items-center justify-center gap-x-2 w-28 py-2 bg-[#0062B9] hover:bg-white hover:text-[#0062B9]'>
                    <FaArrowAltCircleLeft />Previous</button>
                <button className='button flex items-center justify-center gap-x-2 w-28 py-2 bg-[#0062B9] hover:bg-white hover:text-[#0062B9]'>
                    <FaArrowAltCircleRight />Next</button>
            </div>
            <br/>
        </div>
        )
}
export default Course
