import React, { FC } from 'react'
const ChallengeMultipleChoice: FC = () => {
    const [showFirstModal, setShowFirstModal] = React.useState(false);
    const [showSecondModal, setShowSecondModal] = React.useState(false);
    const [showThirdModal, setShowThirdModal] = React.useState(false);
    
    return (
        <div className='w-7/12'>
            <div className='form h-16 py-2'>Challenge Name</div>
            <br/>
            <div className='form text-base grid px-8 py-6'>
                <div>
                    <div className='flex justify-end'>
                        <div className='w-52 h-10 bg-[#455D84]/[.5] flex justify-center gap-x-3 py-2 px-2'>
                            <div>INSTRUCTIONS</div>
                            <div>50XP</div>
                        </div>
                    </div>
                    <br/>
                    <div className='text-left'>1. สวัสดีครับวันนี้มีคำถามมาถามทุกคน คำถามอยู่ที่ว่า ไก่กับไข่อะไรเกิดก่อนกัน</div>
                    <br/>
                    <div className='px-10'>
                        <div className="flex items-center mb-4">
                            <input id="country-option-1" type="radio" name="countries" value="USA" className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300" aria-labelledby="country-option-1" aria-describedby="country-option-1" />
                            <label htmlFor="country-option-1" className="text-sm font-medium text-white ml-2 block">
                            หมา
                            </label>
                        </div>
                        <div className="flex items-center mb-4">
                            <input id="country-option-2" type="radio" name="countries" value="Germany" className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300" aria-labelledby="country-option-2" aria-describedby="country-option-2" />
                            <label htmlFor="country-option-2" className="text-sm font-medium text-white ml-2 block">
                            แมว
                            </label>
                        </div>
                        <div className="flex items-center mb-4">
                            <input id="country-option-3" type="radio" name="countries" value="Spain" className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300" aria-labelledby="country-option-3" aria-describedby="country-option-3" />
                            <label htmlFor="country-option-3" className="text-sm font-medium text-white ml-2 block">
                            ไก่
                            </label>
                        </div>
                        <div className="flex items-center mb-4">
                            <input id="country-option-4" type="radio" name="countries" value="United Kingdom" className="h-4 w-4 border-gray-300 focus:ring-2 focus:ring-blue-300" aria-labelledby="country-option-4" aria-describedby="country-option-4" />
                            <label htmlFor="country-option-4" className="text-sm font-medium text-white ml-2 block">
                            ไข่
                            </label>
                        </div>
                    </div>
                    <br/>
                    <div className='flex gap-2 h-12 justify-between'>
                        <div className='flex gap-2 '>
                        <button className='w-36 bg-[#0062B9] hover:text-[#0062B9] hover:bg-white'>Previous</button>
                        {/* modal */}
                        <button
                            className="w-36 bg-[#0062B9] hover:text-[#0062B9] hover:bg-white"
                            type="button"
                            onClick={() => setShowFirstModal(true)}
                        >
                            Submit
                            {/* <i className='mr-2'>
                                {ButtonIcon}
                            </i> */}
                        </button>
                        </div> 
                        {showFirstModal ? (
                            <>
                            <div
                                className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                            >
                                <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                    <h3 className="text-3xl font-semibold text-black">
                                        Confirm
                                    </h3>
                                    <button
                                        className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                        onClick={() => setShowFirstModal(false)}
                                    >
                                        ×
                                    </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                    <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                        Are you sure you want to sent your answer?
                                    </p>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                    <button
                                        className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                        type="button"
                                        onClick={() => setShowFirstModal(false)}
                                    >
                                        Back
                                    </button>
                                    <button
                                        className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                        type="button"
                                        onClick={() => setShowSecondModal(true)}
                                        // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                                    >
                                        Continue
                                        {/* <i className='mr-2'>
                                            {ButtonIcon}
                                        </i> */}
                                    </button>
                                    {showSecondModal ? (
                                        <>
                                        <div
                                            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                                        >
                                            <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                            {/*content*/}
                                            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                                {/*header*/}
                                                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                                <h3 className="text-3xl font-semibold text-black">
                                                    Thank you
                                                </h3>
                                                <button
                                                    className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                                    // onClick={() => setShowSecondModal(false)}
                                                    onClick={() => [setShowSecondModal(false), setShowFirstModal(false)]}
                                                >
                                                    ×
                                                </button>
                                                </div>
                                                {/*body*/}
                                                <div className="relative p-6 flex-auto">
                                                <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                                    Thank you! Your answer has been send.
                                                </p>
                                                </div>
                                                {/*footer*/}
                                                <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                                <button
                                                    className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                                    type="button"
                                                    // onClick={() => setShowSecondModal(false)}
                                                    onClick={() => [setShowSecondModal(false), setShowFirstModal(false)]}
                                                >
                                                    Continue
                                                </button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                                        </>
                                    ) : null}
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                            </>
                        ) : null}
                       
                        <button
                            className="w-48 h-10 text-yellow-400 px-5 bg-[#344663] outline outline-white"
                            type="button"
                            onClick={() => setShowThirdModal(true)}
                            // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                        >
                            Take Hint
                            <div className='display: inline'>(-15 points)</div>
                            {/* <i className='mr-2'>
                                {ButtonIcon}
                            </i> */}
                        </button>
                        {showThirdModal ? (
                            <>
                            <div
                                className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                            >
                                <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                    <h3 className="text-3xl font-semibold text-black">
                                        Hint
                                    </h3>
                                    <button
                                        className="text-[#FF0505] w-10 text-4xl transition-all duration-150 hover:text-red-800 rounded-full place-content-center"
                                        // onClick={() => setShowSecondModal(false)}
                                        onClick={() => setShowThirdModal(false)}
                                    >
                                        ×
                                    </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                    <p className="my-4 text-slate-500 text-lg text-[#0062B9] leading-relaxed">
                                        From the question i would suggest you to read that again.
                                    </p>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                    <button
                                        className="bg-[#0062B9] text-white active:bg-[#3FA1F9] font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-white hover:text-[#0062B9]"
                                        type="button"
                                        // onClick={() => setShowSecondModal(false)}
                                        onClick={() => setShowThirdModal(false)}
                                    >
                                        Continue
                                    </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                            </>
                        ) : null}
                    </div>
                </div>
            </div>
        </div>
        )
}
export default ChallengeMultipleChoice
