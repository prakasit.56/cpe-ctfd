import { Formik, Form } from "formik";
import { useMutation } from "urql";

import React, { FC } from "react";
import { useLocation } from "react-router-dom";
import Loader from "components/Loader/Loader";

import { useQuery } from "urql";

import { challengeAPI } from "services/graphql";
import {
  ChallengeInterface,
  SubmitFlagChallengeDataInterface,
} from "interfaces/pages/Challenge";
import * as validateSchemaForm from "constants/validateSchemaForm";

const Challenge: FC = () => {
  const [showFirstModal, setShowFirstModal] = React.useState(false);
  const [showSecondModal, setShowSecondModal] = React.useState(false);
  const [showThirdModal, setShowThirdModal] = React.useState(false);

  const search = useLocation().search;
  const challenge_id = new URLSearchParams(search).get("challenge_id");
  const [challenge_by_id] = useQuery(
    challengeAPI.query.getFlagChallengeByID(challenge_id)
  );

  const [SubmitFlagResult, SubmitFlag] = useMutation(
    challengeAPI.mutation.sentFlagChallengeMutation
  );

  const [SentFlagResult, SentFlag] = useMutation(
    challengeAPI.mutation.createChallengeMutation
  );

  const [userData, setUserData] =
    React.useState<SubmitFlagChallengeDataInterface>({
      user_id: "",
      challenge_id: "",
      base_score: "",
      time_score: "",
      level_id: "",
      types_id: "",
      categories_id: "",
      answer: "",
    });

  const initialValues: SubmitFlagChallengeDataInterface = {
    user_id: "",
    challenge_id: "",
    base_score: "",
    time_score: "",
    level_id: "",
    types_id: "",
    categories_id: "",
    answer: "",
  };

  const onClickSubmit = (data: SubmitFlagChallengeDataInterface) => {
    console.log("test");
    challengeAPI.handler.handleOnSubmitFlagChallenge(SubmitFlag, data);
    // console.log(SubmitFlag)
    console.log(SubmitFlagResult);
  };

  return !challenge_by_id.error && !challenge_by_id.fetching ? (
    <div className="mt-10 flex w-full items-center justify-center">
      <div className="w-7/12 rounded-xl px-10 py-8 text-lg">
        <div className="flex justify-end">
          <div className="flex h-10 w-52 justify-center gap-x-3 bg-[#455D84]/[.5] py-2 px-2">
            <div className="text-white">INSTRUCTIONS</div>
            <div className="text-[#FEC751]">
              {challenge_by_id.data.getChallenges[0].scorce}
            </div>
          </div>
        </div>
        <br />
        <div>
          <div>
            <div className="text-left text-lg text-white">
              <div className=" text-2xl text-[#0FB1D9]">
                {challenge_by_id.data.getChallenges[0].name}
              </div>
              <h6 className="">
                {challenge_by_id.data.getChallenges[0].question}
              </h6>
              <h6 className="">
                {challenge_by_id.data.getChallenges[0].description}
              </h6>
            </div>
            <br />
            <br />
            <div>
              <input
                type="text"
                placeholder="Submit your answer here"
                className="h-28 bg-[#455D84]/[.5]"
              />
            </div>
            <br />
            <div className="flex h-16 items-center justify-between rounded-md bg-[#455D84]/[.5] px-2">
              <div className="flex h-12 gap-2">
                <button className="w-36 bg-[#0062B9] hover:bg-white hover:text-[#0062B9] ">
                  Previous
                </button>
                {/* modal */}
                <button
                  className="w-36 bg-[#0062B9] hover:bg-white hover:text-[#0062B9]"
                  type="button"
                  onClick={() => setShowFirstModal(true)}
                >
                  Submit
                  {/* <i className='mr-2'>
                                    {ButtonIcon}
                                </i> */}
                </button>
              </div>
              {showFirstModal ? (
                <>
                  <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
                    <div className="relative my-6 mx-auto w-auto max-w-3xl">
                      {/*content*/}
                      <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                        {/*header*/}
                        <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                          <h3 className="text-3xl font-semibold text-black">
                            Confirm
                          </h3>
                          <button
                            className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                            onClick={() => setShowFirstModal(false)}
                          >
                            ×
                          </button>
                        </div>
                        {/*body*/}
                        <div className="relative flex-auto p-6">
                          <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                            Are you sure you want to sent your answer?
                          </p>
                        </div>
                        {/*footer*/}
                        <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                          <button
                            className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                            type="button"
                            onClick={() => setShowFirstModal(false)}
                          >
                            Back
                          </button>
                          <button
                            className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                            type="button"
                            onClick={() => [
                              setShowSecondModal(true),
                              onClickSubmit,
                            ]}
                            // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                          >
                            Continue
                            {/* <i className='mr-2'>
                                            {ButtonIcon}
                                        </i> */}
                          </button>
                          {showSecondModal ? (
                            <>
                              <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
                                <div className="relative my-6 mx-auto w-auto max-w-3xl">
                                  {/*content*/}
                                  <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                                      <h3 className="text-3xl font-semibold text-black">
                                        Thank you
                                      </h3>
                                      <button
                                        className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                                        // onClick={() => setShowSecondModal(false)}
                                        onClick={() => [
                                          setShowSecondModal(false),
                                          setShowFirstModal(false),
                                        ]}
                                      >
                                        ×
                                      </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative flex-auto p-6">
                                      <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                                        Thank you! Your answer has been send.
                                      </p>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                                      <button
                                        className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                                        type="button"
                                        // onClick={() => setShowSecondModal(false)}
                                        onClick={() => [
                                          setShowSecondModal(false),
                                          setShowFirstModal(false),
                                        ]}
                                      >
                                        Continue
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
                            </>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
                </>
              ) : null}
              {/* Hint */}
              <button
                className="h-10 w-48 bg-[#344663] px-5 text-yellow-400 outline outline-white"
                type="button"
                onClick={() => setShowThirdModal(true)}
                // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
              >
                Take Hint
                <div className="display: inline">(-15 points)</div>
                {/* <i className='mr-2'>
                                {ButtonIcon}
                            </i> */}
              </button>
              {showThirdModal ? (
                <>
                  <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
                    <div className="relative my-6 mx-auto w-auto max-w-3xl">
                      {/*content*/}
                      <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                        {/*header*/}
                        <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                          <h3 className="text-3xl font-semibold text-black">
                            Hint
                          </h3>
                          <button
                            className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                            // onClick={() => setShowSecondModal(false)}
                            onClick={() => setShowThirdModal(false)}
                          >
                            ×
                          </button>
                        </div>
                        {/*body*/}
                        <div className="relative flex-auto p-6">
                          <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                            {
                              challenge_by_id.data.getChallenges[0]
                                .flagQuestions[0].hint
                            }
                          </p>
                        </div>
                        {/*footer*/}
                        <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                          <button
                            className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                            type="button"
                            // onClick={() => setShowSecondModal(false)}
                            onClick={() => setShowThirdModal(false)}
                          >
                            Continue
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
                </>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <Loader />
  );
};
export default Challenge;
