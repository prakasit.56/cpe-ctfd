import React from 'react'
import Table, { AvatarCell} from './components/Table'
import { DotLeft, DotRight } from "components/Dot";
import Loader from 'components/Loader/Loader'

import { useQuery } from 'urql'

import { challengeAPI } from 'services/graphql';

function MainChallenge() {

  const [challenge] = useQuery(challengeAPI.query.getChallenge())
  // console.log(challenge.data.getChallenges[0].name)

  let link
  const check_challenge = (e) => {
    if(e == 'Multiple Choice')
      link = '/challenge-multiple-choice';
    if(e == 'Short Answer')
      link = '/challenge-short-answer';
    if(e == 'Flag')
      link = '/flag-challenge';

    return link
  }

  const columns = React.useMemo(() => [
    {
      Header: "Challenge",
      accessor: 'name',
      // Cell: AvatarCell,
      // Cell: props => <button className="btn1" onClick={() => {href props.row.original.name}}>{props.row.original.name}</button>,
      Cell: props => 
        <div>
          <button className="btn1" onClick={() => window.location.href='' + check_challenge(props.row.original.types.name) + `?challenge_id=${props.row.original.challenge_id}`}>{props.row.original.name}</button>
          {/* <button className="btn1" onClick={() => console.log(props.row.original)}>{props.row.original.name}</button> */}
        </div>,
      imgAccessor: "imgUrl",
      emailAccessor: "email",
    },
    {
      Header: "Level",
      accessor: 'levels.name',
    },
    // {
    //   Header: "Status",
    //   accessor: 'status',
    //   Cell: StatusPill,
    // },
    {
      Header: "Score",
      accessor: 'scorce',
    },
    {
      Header: "Type",
      accessor: 'types.name',
    //   Filter: SelectColumnFilter,  // new
      filter: 'includes',
    },
  ], [])
  let challenge_data
  if (!challenge.error && !challenge.fetching){
    challenge_data = challenge.data.getChallenges
  }

  return (
    !challenge.error && !challenge.fetching ?(
    <div className="min-h-screen text-white pb-8 mt-10">
      <main className="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8 pt-4">
        <div className="">
          <h1 className="text-3xl text-center">Challenge</h1>
        </div>
        <div className="mt-6">
          <Table columns={columns} data={challenge_data} />
        </div>
      </main>
      {/* <DotLeft className="bottom-0" />
      <DotRight className="" /> */}
    </div>
    ) : <Loader />
  );
}

export default MainChallenge;
