import { Formik, Form } from "formik";
import { useMutation } from "urql";

import React, { FC } from "react";

import * as form from "components/Form";
import { challengeAPI } from "services/graphql";
import { CreateShortAnswerChallengeDataInterface } from "interfaces/pages/Challenge";
import * as validateSchemaForm from "constants/validateSchemaForm";

const CreateShortAnswer: FC = () => {
  const [showFirstModal, setShowFirstModal] = React.useState(false);
  const [showSecondModal, setShowSecondModal] = React.useState(false);
  const [showThirdModal, setShowThirdModal] = React.useState(false);

  const [CreateShortResult, CreateShort] = useMutation(
    challengeAPI.mutation.createChallengeMutation
  );

  const handleSubmit = (data: CreateShortAnswerChallengeDataInterface) => {
    challengeAPI.handler.handleOnSubmitCreateShortAnswerChallenge(
      CreateShort,
      data
    );
    console.log("CreateShortResult");
    setShowFirstModal(false);
    console.log(CreateShortResult);
  };

  const initialValues: CreateShortAnswerChallengeDataInterface = {
    name: "",
    question: "",
    description: "",
    answer: "",
    hint: "",
  };

	let current_value
    const ChangeToPage = (event) => {
        current_value = event.target.value || "#"
        console.log(current_value)
        window.location.href='/' + (event.target.value);
    };

    const CheckValue = (event) => {
        current_value = event.target.value || "#"
        console.log(current_value)
    };

  return (
    <div className="form flex h-4/5 w-8/12 px-12 py-8 pb-16 text-lg">
      <div className="grid text-3xl">
        Create Challenge
        <br />
        <h1 className="text-[#0FB1D9]">Short Answer</h1>
      </div>
      <br />
      <br />
      <Formik
        enableReinitialize
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validateSchemaForm.CreateShortAnswerChallengeValidationSchema()}
      >
        {({ errors, touched }) => (
          <Form id="addShortAnswerChallenge">
            <div className="flex justify-between gap-20 text-left">
              <div className="grid w-9/12 gap-y-4">
                {form.DataFieldForm.CreateShortDataFieldForm().map(
                  (formData) => (
                    <form.CreateChallengeFieldForm
                      key={formData.name}
                      touched={touched[formData.name]}
                      errors={errors[formData.name]}
                      label={`${formData.label}`}
                      name={formData.name}
                      type={formData.type}
                      placeholder={formData.placeholder}
                    />
                  )
                )}
              </div>
            </div>
          </Form>
        )}
      </Formik>
      <br />
      <div className="flex gap-3">
				<button onClick={() => window.location.href='/create-challenge'} className="w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9]">
					Back
				</button>
        <button
          className="w-32 bg-[#166615] py-2 hover:bg-white hover:text-[#166615]"
          type="button"
          onClick={() => setShowThirdModal(true)}
          // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
        >
          Submit
          {/* <i className='mr-2'>
                        {ButtonIcon}
                    </i> */}
        </button>
        {showThirdModal ? (
          <>
            <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
              <div className="relative my-6 mx-auto w-auto max-w-3xl">
                {/*content*/}
                <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                    <h3 className="text-3xl font-semibold text-black">
                      Confirm
                    </h3>
                    <button
                    	className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                      // onClick={() => setShowSecondModal(false)}
                      onClick={() => setShowThirdModal(false)}
                    ></button>
                  </div>
                  {/*body*/}
                  <div className="relative flex-auto p-6">
                    <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                      Are you sure you want to send your challenge to admin ?
                    </p>
                  </div>
                  {/*footer*/}
                  <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                    <button
                      className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                      type="button"
                      // onClick={() => setShowSecondModal(false)}
                      onClick={() => setShowThirdModal(false)}
                    >
                      Back
                    </button>
                    <button
											form="addShortAnswerChallenge"
                      className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                      type="submit"
                      // onClick={() => setShowSecondModal(false)}
                    >
                      Continue
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
          </>
        ) : null}
        {showFirstModal ? (
          <>
            <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
              <div className="relative my-6 mx-auto w-auto max-w-3xl">
                {/*content*/}
                <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                    <h3 className="text-3xl font-semibold text-black">
                      Caution
                    </h3>
                    <button
                      className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                      onClick={() => setShowFirstModal(false)}
                    >
                      ×
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative flex-auto p-6">
                    <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                      Are you sure to you want to delete this question?
                    </p>
                  </div>
                  {/*footer*/}
                  <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                    <button
                      className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                      type="button"
                      onClick={() => setShowFirstModal(false)}
                    >
                      Back
                    </button>
                    <button
                      className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                      type="button"
                      onClick={() => setShowSecondModal(true)}
                      // onClick={() => [setShowSecondModal(true), setShowFirstModal(false)]}
                    >
                      Continue
                      {/* <i className='mr-2'>
                                    {ButtonIcon}
                                </i> */}
                    </button>
                    {showSecondModal ? (
                      <>
                        <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
                          <div className="relative my-6 mx-auto w-auto max-w-3xl">
                            {/*content*/}
                            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                              {/*header*/}
                              <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                                <h3 className="text-3xl font-semibold text-black">
                                  Deleted
                                </h3>
                                <button
                                  className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                                  // onClick={() => setShowSecondModal(false)}
                                  onClick={() => [
                                    setShowSecondModal(false),
                                    setShowFirstModal(false),
                                  ]}
                                >
                                  ×
                                </button>
                              </div>
                              {/*body*/}
                              <div className="relative flex-auto p-6">
                                <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                                  Your Question has been deleted.
                                </p>
                              </div>
                              {/*footer*/}
                              <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                                <button
                                  className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                                  type="button"
                                  // onClick={() => setShowSecondModal(false)}
                                  onClick={() => [
                                    setShowSecondModal(false),
                                    setShowFirstModal(false),
                                  ]}
                                >
                                  Continue
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
                      </>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
            <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
          </>
        ) : null}
      </div>
      <br />
      <br />
    </div>
  );
};
export default CreateShortAnswer;
