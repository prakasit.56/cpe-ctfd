import React from "react";
import Table from "../components/Table";
import { DotLeft, DotRight } from "components/Dot";
import Loader from "components/Loader/Loader";

import { useQuery } from "urql";

import { userAPI } from "services/graphql";

function ScoreLeaderboard() {
  const [user_score] = useQuery(userAPI.score_query.getUserScore());

  const columns = React.useMemo(
    () => [
      {
        Header: "Rank",
        accessor: "rank",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Score",
        accessor: "totalScore",
      },
      {
        Header: "Challenge Clear",
        accessor: "numChallenge",
      },
    ],
    []
  );

  let time_score_leaderboard_data;
  if (!user_score.error && !user_score.fetching) {
    time_score_leaderboard_data = user_score.data.groupByUserScores;
    time_score_leaderboard_data = time_score_leaderboard_data.map((e, i) => {return ({...e, rank: i+1})})
  }

  return !user_score.error && !user_score.fetching ? (
    <div className="mt-10 min-h-screen pb-8 text-white">
      <main className="mx-auto max-w-5xl px-4 pt-4 sm:px-6 lg:px-8">
        <div className="">
          <h1 className="text-3xl text-center">Time Leaderboard</h1>
        </div>
        <div className="mt-6">
          <Table columns={columns} data={time_score_leaderboard_data} />
        </div>
      </main>
    </div>
  ) : (
    <Loader />
  );
}

export default ScoreLeaderboard;
