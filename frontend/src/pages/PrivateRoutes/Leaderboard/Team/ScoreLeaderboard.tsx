import React from "react";
// import Table, { AvatarCell, SelectColumnFilter, StatusPill } from './components/Table'  // new
import Table, { AvatarCell } from "../components/Table";
import { DotLeft, DotRight } from "components/Dot";

import Loader from "components/Loader/Loader";
import { useQuery } from "urql";

import { teamAPI } from "services/graphql";

function ScoreLeaderboard() {
  const [team] = useQuery(teamAPI.query.getTeamScore());
  let data2;
  if (!team.error && !team.fetching) {
    data2 = team.data.getTeams;
    console.log(data2);
  }

  const columns = React.useMemo(
    () => [
      {
        Header: "Rank",
        accessor: "challengeName",
      },
      {
        Header: "Team Name",
        Cell: (props) => (
          <div>
            <button
              className="btn1"
              onClick={() => (window.location.href = "/team-profile")}
            >
              {props.row.original.name}
            </button>
          </div>
          ),
      },
      // {
      //   Header: "Level",
      //   accessor: "level",
      // },
      // {
      //   Header: "Status",
      //   accessor: 'status',
      //   Cell: StatusPill,
      // },
      {
        Header: "Score",
        accessor: "score",
        Cell: (props) => {
          console.log(props);
          return (
            <div>
              {props.row.original.teamMembers.reduce(
                (previousValue, currentValue) =>
                  previousValue +
                  currentValue.users.userScores.reduce(
                    (previousValue, currentValue) =>
                      previousValue + currentValue.base_score,
                    0
                  ),
                0
              )}
            </div>
          );
        },
      },
      {
        Header: "Challenge Clear",
        //   Filter: SelectColumnFilter,  // new
        filter: "includes",
        Cell: (props) => {
          console.log(props);
          return (
            <div>
              {props.row.original.teamMembers.reduce(
                (previousValue, currentValue) =>
                  previousValue + currentValue.users.userScores.length,
                0
              )}
            </div>
          );
        },
      },
    ],
    []
  );

  return !team.error && !team.fetching ? (
    <div className="mt-10 min-h-screen pb-8 text-white">
      <main className="mx-auto max-w-5xl px-4 pt-4 sm:px-6 lg:px-8">
        <div className="">
          <h1 className="text-3xl text-center">Team Leaderboard</h1>
        </div>
        <div className="mt-6">
          <Table columns={columns} data={data2} />
        </div>
      </main>
      {/* <DotLeft className="bottom-0" />
      <DotRight className="" /> */}
    </div>
  ) : (
    <Loader />
  );
}

export default ScoreLeaderboard;
