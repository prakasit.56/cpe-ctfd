import { Formik, Form, Field, ErrorMessage } from "formik";
import { useMutation } from "urql";

import React, {
  useState,
  useMemo,
} from "react";
import countryList from "react-select-country-list";
import Loader from "components/Loader/Loader";

import { useQuery } from "urql";

import { userAPI } from "services/graphql";

import { UserInterface } from "interfaces/pages/Users";
import * as form from "components/Form";
import * as validateSchemaForm from "constants/validateSchemaForm";

const ProfileSetting = (props) => {
  const [showFirstModal, setShowFirstModal] = React.useState(false);
  const [showSecondModal, setShowSecondModal] = React.useState(false);
  // const [showThirdModal, setShowThirdModal] = React.useState(false);

  const [user] = useQuery(userAPI.query.getUser());
  
  const [ProfielUpdateResult, ProfileUpdate] = useMutation(
    userAPI.mutation.updateUserMutation
  );


  const handleSubmit = (data: UserInterface) => {
    userAPI.handler.handleOnSubmitProfileSettingSaveChange(ProfileUpdate, data);
    setShowSecondModal(false)
  };

  const [value, setValue] = useState("");
  const changeHandler = (value) => {
    setValue(value);
  };

  const options = useMemo(() => countryList().getData(), []);
  const customStyles = {
    valueContainer: (provided, state) => ({
      ...provided,
      borderradius: "8px",
      fontsize: "small",
      padding: "0.625 rem",
    }),

    control: (styles, state) => ({
      ...styles,
      height: "45px",
      backgroundColor: "#1A2332",
      boxShadow: state.isFocused ? "#1A2332" : "#1A2332",
      borderColor: state.isFocused ? "#1A2332" : "#1A2332",
      "&:hover": {
        borderColor: state.isFocused ? "white" : "white",
      },
    }),

    option: (provided, state) => ({
      ...provided,
      padding: 20,
      color: state.isSelected ? "white" : "black",
    }),

    singleValue: (provided) => ({
      ...provided,
      color: "white",
      padding: "0.625rem",
    }),

    placeholder: (defaultStyles) => {
      return {
        ...defaultStyles,
        color: "grey",
      };
    },

    input: (base, state) => ({
      ...base,
      color: "white",
      marginTop: "-2px",
    }),
  };
  
  let initialValues: UserInterface;
  if (!user.error && !user.fetching) {
    initialValues = {
      name: user.data.getUsers[0].name,
      email: user.data.getUsers[0].email,
      conuntry_code: user.data.getUsers[0].conuntry_code,
      github_link: user.data.getUsers[0].github_link,
      twitter_link: user.data.getUsers[0].twitter_link,
      facebook_link: user.data.getUsers[0].facebook_link,
    };
  }

  return !user.error && !user.fetching ? (
    <div className={"mt-10 flex h-3/5 w-full justify-center"}>
      <div className="form font-Roboto w-7/12 rounded-xl px-6 py-4 pb-10 text-white">
        <div className="flex text-3xl">Setting</div>
        <div className="field gap-4">
          {/* profile picture */}
          <img
            src={user.data.getUsers[0].profile_pic}
            className="h-24 w-24"
          ></img>
          <div>
            <div className="flex">Profile Avatar</div>
            <br />
            <div className="flex items-end gap-4">
              <button
                type="button"
                className={`w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9]`}
              >
                Upload Avatar
              </button>
              <button
                className={`w-16 bg-[#BF0000] py-2 hover:bg-white hover:text-[#BF0000]`}
                type="button"
                onClick={() => setShowFirstModal(true)}
              >
                Delete
              </button>
            </div>
          </div>
        </div>
        <Formik
          enableReinitialize
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validateSchemaForm.ProfileSettingValidationSchema()}
        >
          {({ errors, touched }) => (
            <Form id="editprofile">
              <div className="field flex gap-20 text-white">
                <div className="grid text-left">
                  {form.DataFieldForm.EditProfileFieldForm().map((formData) => (
                    <form.EditProfileForm
                      key={formData.name}
                      touched={touched[formData.name]}
                      errors={errors[formData.name]}
                      label={`${formData.label}`}
                      name={formData.name}
                      type={formData.type}
                      placeholder={formData.placeholder}
                    />
                  ))}
                </div>
              </div>
            </Form>
          )}
        </Formik>
        <div className="grid justify-items-end">
          <button
            className={`w-28 bg-[#0062B9] py-2 hover:bg-white hover:text-[#0062B9] `}
            type="button"
            onClick={() => setShowSecondModal(true)}
          >
            Save Changes
          </button>
        </div>
      </div>
      {showFirstModal ? (
        <>
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
            <div className="relative my-6 mx-auto w-auto max-w-3xl">
              {/*content*/}
              <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                  <h3 className="text-3xl font-semibold text-black">Caution</h3>
                  <button
                    className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                    onClick={() => setShowFirstModal(false)}
                  >
                    ×
                  </button>
                </div>
                {/*body*/}
                <div className="relative flex-auto p-6">
                  <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                    Are you sure to you want to delete profile picture?
                  </p>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                  <button
                    className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                    type="button"
                    onClick={() => setShowFirstModal(false)}
                  >
                    Back
                  </button>
                  <button
                    className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                    onClick={() => setShowFirstModal(false)}
                    type="button"
                    >
                    Continue
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
        </>
      ) : null}
      {showSecondModal ? (
        <>
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
            <div className="relative my-6 mx-auto w-auto max-w-3xl">
              {/*content*/}
              <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between rounded-t border-b border-solid border-slate-200 p-5">
                  <h3 className="text-3xl font-semibold text-black">Confirm</h3>
                  <button
                    className="w-10 place-content-center rounded-full text-4xl text-[#FF0505] transition-all duration-150 hover:text-red-800"
                    onClick={() => setShowSecondModal(false)}
                  >
                    ×
                  </button>
                </div>
                {/*body*/}
                <div className="relative flex-auto p-6">
                  <p className="my-4 text-lg leading-relaxed text-slate-500 text-[#0062B9]">
                    Your data has been saved
                  </p>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end rounded-b border-t border-solid border-slate-200 p-6">
                  <button
                    className="mr-1 mb-1 rounded bg-[#0062B9] px-6 py-3 text-sm font-bold text-white shadow outline-none transition-all duration-150 ease-linear hover:bg-white hover:text-[#0062B9] hover:shadow-lg focus:outline-none active:bg-[#3FA1F9]"
                    form="editprofile"
                    type="submit"
                  >           
                    Continue
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
        </>
      ) : null}
    </div>
  ) : (
    <Loader />
  );
};

export default ProfileSetting;
