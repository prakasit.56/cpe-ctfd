import { FC } from 'react'
import RadarChart from 'react-svg-radar-chart';
import 'react-svg-radar-chart/build/css/index.css'
import { RiAccountCircleFill } from "react-icons/ri";
import { BiWorld } from 'react-icons/bi';
import { AiOutlineGithub } from 'react-icons/ai';
import { GiArrowScope } from 'react-icons/gi';
import { GrFacebook } from 'react-icons/gr';
import { BsTwitter } from 'react-icons/bs';
import Loader from 'components/Loader/Loader'

import { useQuery } from 'urql'

import { userAPI } from 'services/graphql';

const data = [
    {
        data: {
        hardware: 0.7,
        reversing: .8,
        misc: 0.9,
        osint: 0.67,
        foreignsicst: 0.8,
        pwn: 0.7,
        web: .8,
        mobile: 0.9,
        stego: 0.67,
        crypto: 0.8
    },
        meta: { color: 'purple' }
    },
];

const captions = {
    // columns
    hardware: 'Hardware',
    reversing: 'Reversing',
    misc: 'MISC',
    osint: 'OSINT',
    foreignsicst: 'Foreignsics',
    pwn: 'PWN',
    web: 'Web',
    mobile: 'Mobile',
    stego: 'Stego',
    crypto: 'Crypto'
};

const noSmoothing = points => {
    let d = 'M' + points[0][0].toFixed(4) + ',' + points[0][1].toFixed(4);
    for (let i = 1; i < points.length; i++) {
      d += 'L' + points[i][0].toFixed(4) + ',' + points[i][1].toFixed(4);
    }
    return d + 'z';
  };
   
  const defaultOptions = {
    size: 200,
    axes: true, // show axes?
    scales: 3, // show scale circles?
    captions: true, // show captions?
    captionMargin: 10,
    dots: true, // show dots?
    zoomDistance: 1.2, // where on the axes are the captions?
    setViewBox: (options) => `-${options.captionMargin} 0 ${options.size + options.captionMargin * 2} ${options.size}`, // custom viewBox ?
    smoothing: noSmoothing, // shape smoothing function
    axisProps: () => ({ className: 'axis' }),
    scaleProps: () => ({ className: 'scale', fill: 'none' }),
    shapeProps: () => ({ className: 'shape' }),
    captionProps: () => ({
        className: 'caption',
        textAnchor: 'middle',
        fontSize: 10,
        fontFamily: 'sans-serif'
    }),
    dotProps: () => ({
        className: 'dot',
        mouseEnter: (dot) => { 
            document.getElementById("tooltip").innerText = "index: " + dot.idx + ", key: " + dot.key + ", value: " + dot.value;
            document.getElementById("tooltip").style.visibility = "visible";
        },
        mouseLeave: (dot) => { 
            document.getElementById("tooltip").innerText = "";
            document.getElementById("tooltip").style.visibility = "hidden";
        }
    })
  };

const Profile = (props) => {

    const [user] = useQuery(userAPI.query.getUser())
    console.log(user.data)

    return (
        !user.error && !user.fetching ?(
        <div className='mt-10 h-4/5 w-full flex justify-center text-3xl'>
            <div className='form w-8/12 px-10 py-8 gap-y-2 flex'>
                <div className='flex items-center justify-between'> 
                    Profile
                    <button
                        type="button" 
                        className='bg-[#0062B9] w-20 h-10 hover:bg-white hover:text-[#0062B9] text-base'
                        onClick={() => window.location.href='/profile-setting' + `?profile_id=${user.data.getUsers[0].user_id}`}>
                            Setting
                    </button>
                </div>

                <div className='flex justify-between'>
                    <div className='flex text-base gap-4 items-start'>
                        <RiAccountCircleFill className='w-24 h-24' />
                        <div className='grid gap-y-5 py-4'> 
                            {user.data.getUsers[0].name}
                            <br/>
                            <div className='flex items-center gap-1'>
                                <BiWorld />
                                {user.data.getUsers[0].conuntry_code}
                            </div>
                        </div>
                    </div>
                    <div className='bg-zinc-600/[.3] w-64 h-52 rounded-md items-center py-16 text-center'>
                        <div>RANKING</div>
                        <div>{user.data.getUsers[0].total_score} #</div>
                    </div>
                </div>

                <div className='flex'>
                    Staticstic
                </div>

                <div className='flex justify-between'>
                <div className='item-end'>
                <RadarChart
                        captions={captions}
                        data={data}
                        size={450}
                        options={defaultOptions}/>
                </div>
                    <div className='py-28 px-6 text-center'>
                        <div className='flex gap-x-2 text-xl justify-center'>
                            <GiArrowScope className='text-3xl'/>
                            {user.data.getUsers[0].total_score}
                            <br/>
                            Scores
                        </div>
                        <br/>
                        <div className='flex gap-x-10 text-5xl'>
                            <GrFacebook 
                                onClick={() => window.location.href=user.data.getUsers[0].facebook_link}
                                style={{cursor: "pointer"}}
                            />
                            <AiOutlineGithub 
                                onClick={() => window.location.href=user.data.getUsers[0].github_link}
                                style={{cursor: "pointer"}}
                            />
                            <BsTwitter 
                                onClick={() => window.location.href=user.data.getUsers[0].twitter_link}
                                style={{cursor: "pointer"}}
                            />
                        </div>
                        <br/>
                        <br/>
                        <button 
                            type="button" 
                            className='bg-[#0062B9] w-32 h-10 hover:bg-white hover:text-[#0062B9] text-base '
                            onClick={() => window.location.href='/badge' + `?profile_id=${user.data.getUsers[0].user_id}`}>
                                View Badges
                        </button>
                    </div>
                </div>
            </div>
        </div>
        ) : <Loader />
    )
}
export default Profile
