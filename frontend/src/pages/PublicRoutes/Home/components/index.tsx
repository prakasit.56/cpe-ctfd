import WelcomeSection from "pages/PublicRoutes/Home/components/WelcomeSection";
import FeaturesSection from "pages/PublicRoutes/Home/components/FeaturesSection";
import MainEvent from "pages/PublicRoutes/MainEvent/MainEventPage";

export { WelcomeSection, FeaturesSection, MainEvent };
