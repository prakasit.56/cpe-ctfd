import { selector } from 'recoil'
import {
  loadingAtom,
} from 'store/recoil/application/atom'

const loadingSelector = selector({
  key: 'loadingSelector',
  get: ({ get }) => get(loadingAtom).loading,
  set: ({ set }, newValue) => {
    set(loadingAtom, { loading: newValue })
  },
})

export { loadingSelector }
