import * as userAPI from "services/graphql/users"
import * as teamAPI from "services/graphql/teams"
import * as challengeAPI from "services/graphql/challenges"
import * as badgeAPI from "services/graphql/badges"
import * as tutorialAPI from "services/graphql/tutorials"

export { userAPI, teamAPI, challengeAPI, badgeAPI, tutorialAPI }
