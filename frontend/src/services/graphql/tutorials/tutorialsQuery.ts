const getUserTutorial = () => {
  const getgetUserTutorialQuery = `
    query getUserTutorials{
      getUserTutorials {
        user_id
        tutorialChapterDatas
        {
          chapter_id
          tutorial_id
          tutorialContents
          {
            name
          }
          chapter_name
          description
          _count {
            userTutorials
          }
        }
      }
    }
        `
  return {
    query: getgetUserTutorialQuery,
    variables: {
      where: {
        user_id: {
          equals: "",
        },
      },
    },
  }
}

const getTutorial = () => {
  const getgetTutorialContents = `
  query TutorialContents{
    getTutorialContents{
      tutorial_id
      name
      tutorialChapterDatas{
        chapter_id
        chapter_name
        description
      }
    }
  }`

  return {
    query: getgetTutorialContents,

    variables: {
      where: {
        tutorial_id: {
          equals: "",
        },
      },
    },
  }
}

export { getUserTutorial, getTutorial }
