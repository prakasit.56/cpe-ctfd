import * as handler from 'services/graphql/teams/teamsHandler'
import * as query from 'services/graphql/teams/teamsQuery'
import * as mutation from 'services/graphql/teams/teamsMutation'

export { handler, query, mutation }
