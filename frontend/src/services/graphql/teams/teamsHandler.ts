import { CreateTeamInterface } from 'interfaces/pages/Team'

import { general } from 'utils'
import { PagesURL } from 'constants/index'

const handleOnSubmitUploadTeamProfilePicture = (
  Team: any,
  value: CreateTeamInterface,
) => {
  const variables = {
    data: {
      team_profile_pic: value.team_profile_pic,
    },
  }

  /* A way to handle the error. */
  Team(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }
  })
}

const handleOnSubmitCreateTeam = (
  CreateTeam: any,
  value: CreateTeamInterface,
) => {
  const variables = {
    data: {
      team_profile_pic: value.team_profile_pic,
      name: value.name,
      country_code: value.country_code,
      github_link: value.github_link,
      twitter_link: value.twitter_link,
      facebook_link: value.facebook_link,
    },
  }

  /* A way to handle the error. */
  CreateTeam(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.TEAM_PROFILE)
  })
}

const handleOnSubmitDeleteTeamProfilePicture = (
  Team: any,
  value: CreateTeamInterface,
) => {
  const variables = {
    data: {
      team_profile_pic: '',
    },
  }

  /* A way to handle the error. */
  Team(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.TEAM_PROFILE)
  })
}

const handleOnSubmitDeleteProfilePicture = (
  CreateTeam: any,
  value: CreateTeamInterface,
) => {
  const variables = {
    data: {
        team_profile_pic: '',
    },
  }

  /* A way to handle the error. */
  CreateTeam(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      if (code === 401) {
        console.log('Please handle log righ here for 401')
        // By pass the setState of the modal which you want to show in here
        /*
        setDialogModalData({
          title: t('error_msg.user.user_dialog_message.login.unexpeted_error'),
          status: 'error',
          show: true,
          submitBtn: general.http.refresh,
        })
        */
        return
      }

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    let tokens = res.data.login.refresh_token
    general.auth.setRefreshToken(tokens)

    general.http.Redirect()
  })
}

const handleOnSubmitSaveChanges = (
    CreateTeam: any,
    value: CreateTeamInterface,
  ) => {
    const variables = {
      data: {
        team_profile_pic: value.team_profile_pic,
        name: value.name,
        country_code: value.country_code,
        github_link: value.github_link,
        twitter_link: value.twitter_link,
        facebook_link: value.facebook_link,
      },
    }
  
    /* A way to handle the error. */
    CreateTeam(variables).then((res) => {
      if (res.error) {
        const error: any = res.error
        const code: number = error.graphQLErrors[0].originalError.code
  
        if (code === 401) {
          console.log('Please handle log righ here for 401')
          // By pass the setState of the modal which you want to show in here
          /*
          setDialogModalData({
            title: t('error_msg.user.user_dialog_message.login.unexpeted_error'),
            status: 'error',
            show: true,
            submitBtn: general.http.refresh,
          })
          */
          return
        }
  
        // You can add more error handdler in each code
        if (code) {
          console.log('Please handle log righ here')
          return
        }
      }
  
      let tokens = res.data.login.refresh_token
      general.auth.setRefreshToken(tokens)
  
      general.http.Redirect()
    })
  }

export {
  handleOnSubmitUploadTeamProfilePicture,
  handleOnSubmitCreateTeam,
  handleOnSubmitDeleteTeamProfilePicture,
  handleOnSubmitDeleteProfilePicture,
  handleOnSubmitSaveChanges,
}
