import { ChallengeInterface, CreateFlagChallengeDataInterface, SubmitFlagChallengeDataInterface, CreateShortAnswerChallengeDataInterface } from 'interfaces/pages/Challenge'

import { general } from 'utils'
import { PagesURL } from 'constants/index'

const handleOnSelectChallengeType = (
  ChallengeType: any,
  value: ChallengeInterface,
) => {
  const variables = {
    data: {
        types: value.types,
    },
  }

  /* A way to handle the error. */
  ChallengeType(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.MAIN_CHALLENGE)
  })
}

const handleOnSubmitCreateChallenge = (
  CreateChallenge: any,
  value: ChallengeInterface,
) => {
  const variables = {
    data: {
        name: value.name,
        question: value.question,
        description: value.description,
        levels: value.levels,
        level_id: value.level_id,
        types: value.types,
        types_id: value.types_id,
        categories: value.categories,
        categories_id: value.categories_id,
        badges: value.award_badge,
        scorce: value.scorce,
        max_time: value.max_time,
        max_scorce: value.max_scorce,
    },
  }

  /* A way to handle the error. */
  CreateChallenge(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.MAIN_CHALLENGE)
  })
}

const handleOnSubmitCreateFlagChallenge = (
  CreateFlagChallenge: any,
  value: CreateFlagChallengeDataInterface,
) => {
  const variables = {
    data: {
        name: value.name,
        question: value.question,
        description: value.description,
        flagQuestions: {
          create: {
            video_link: value.video_link,
            description: "",
            question: "",
            answer: value.answer,
            hint: value.hint,
        }
        }
    },
  }

  /* A way to handle the error. */
  CreateFlagChallenge(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please Insert All Input')
        return
      }
    }

    general.http.Goto(PagesURL.MAIN_CHALLENGE)
  })
}

const handleOnSubmitCreateMutipleChallenge = (
  CreateChallenge: any,
  value: ChallengeInterface,
) => {
  const variables = {
    data: {
        name: value.name,
        question: value.question,
        description: value.description,
        levels: value.levels,
        level_id: value.level_id,
        types: value.types,
        types_id: value.types_id,
        categories: value.categories,
        categories_id: value.categories_id,
        badges: value.award_badge,
        scorce: value.scorce,
        max_time: value.max_time,
        max_scorce: value.max_scorce,
    },
  }

  /* A way to handle the error. */
  CreateChallenge(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.MAIN_CHALLENGE)
  })
}

const handleOnSubmitCreateShortAnswerChallenge = (
  CreateShortAnswerChallenge: any,
  value: CreateShortAnswerChallengeDataInterface,
) => {
  const variables = {
    data: {
        name: value.name,
        question: value.question,
        description: value.description,
        shortAnswerQuestions: {
          create: {
            question: value.question,
            hint: value.hint,
            answer: value.answer,
          }
        }
    },
  }

  /* A way to handle the error. */
  CreateShortAnswerChallenge(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.MAIN_CHALLENGE)
  })
}

const handleOnSubmitFlagChallenge = (
  FlagChallenge: any,
  value: SubmitFlagChallengeDataInterface,
) => {
  const variables = {
    data: {
      user_id: value.user_id,
      challenge_id: value.challenge_id,
      base_score: value.base_score,
      time_score: value.time_score,
      level_id: value.level_id,
      types_id: value.types_id,
      categories_id: value.categories_id,
      answer: value.answer,
    },
  }

  /* A way to handle the error. */
  FlagChallenge(variables).then((res) => {
    if (res.error) {
      const error: any = res.error
      const code: number = error.graphQLErrors[0].originalError.code

      if (code === 401) {
        console.log('Please handle log righ here for 401')
        // By pass the setState of the modal which you want to show in here
        /*
        setDialogModalData({
          title: t('error_msg.user.user_dialog_message.login.unexpeted_error'),
          status: 'error',
          show: true,
          submitBtn: general.http.refresh,
        })
        */
        return
      }

      // You can add more error handdler in each code
      if (code) {
        console.log('Please handle log righ here')
        return
      }
    }

    general.http.Goto(PagesURL.MAIN_CHALLENGE)
  })
}

export {
  handleOnSelectChallengeType,
  handleOnSubmitCreateChallenge,
  handleOnSubmitCreateFlagChallenge,
  handleOnSubmitCreateShortAnswerChallenge,
  handleOnSubmitFlagChallenge
}
