const createChallengeMutation = `
  mutation createChallenges ($data: ChallengesCreateInput!) {
    createChallenges (data: $data) {
      challenge_id
  }
}`

const createFlagChallengeMutation = `
  mutation createFlagQuestions ($data: FlagQuestionsCreateInput!) {
    createFlagQuestions (data: $data) {
        challenge_id
        flag_question_id
    }
  }`

const createMultipleQuestionChallengeMutation = `
  mutation createMutipleQuestions ($data: MultipleQuestionsCreateInput!) {
    createMutipleQuestions (data: $data) {
        challenge_id
    }
  }`

const createMultipleChoiceMutation = `
  mutation createMultipleChoiceQuestion ($data: MultipleChoiceQuestionCreateInput!) {
    createMultipleChoiceQuestion (data: $data) {
        challenge_id
    }
  }`

const createShortAnswerChallengeMutation = `
  mutation createShortAnswerQuestions ($data: ShortAnswerQuestionsCreateInput!) {
    createShortAnswerQuestions (data: $data) {
        challenge_id
    }
  }`

const sentFlagChallengeMutation = `
  mutation createShortAnswerQuestions ($data: ShortAnswerQuestionsCreateInput!) {
    createShortAnswerQuestions (data: $data) {
    }
  }`

const sentMultipleQuestionChallengeMutation = `
  mutation createShortAnswerQuestions ($data: ShortAnswerQuestionsCreateInput!) {
    createShortAnswerQuestions (data: $data) {
    }
  }`

const sentShortAnswerChallengeMutation = `
  mutation createShortAnswerQuestions ($data: ShortAnswerQuestionsCreateInput!) {
    createShortAnswerQuestions (data: $data) {
    }
  }`

export {
  createChallengeMutation,
  createFlagChallengeMutation,
  createMultipleQuestionChallengeMutation,
  createMultipleChoiceMutation,
  createShortAnswerChallengeMutation,
  sentFlagChallengeMutation,
  sentMultipleQuestionChallengeMutation,
  sentShortAnswerChallengeMutation
}
