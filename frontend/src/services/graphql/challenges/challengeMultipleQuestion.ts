const getChallengeMultiple = () => {
    const getgetChallengeMultipleQuery = `
      query getMutipleQuestions{
        getMutipleQuestions {
            multiple_question_id
            challenges {
                challenge_id
                name
                question
                description
                scorce
            }
            multipleChoiceQuestion {
                answer
                correct_flag
            }
            hint
        }
    }
          `

    return {
      query: getgetChallengeMultipleQuery,
      variables: {
        where: {
          challenge_id: {
            equals: ""
          }
        }
      },
    }
  }

  
  export { getChallengeMultiple }
  