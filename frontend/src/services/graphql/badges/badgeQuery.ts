const getBadge = () => {
  const getgetUserBadgeQuery = `
    query getUserBadges{
      getUserBadges {
          user_id
          badge_id
          badges
          {
            name
            description
          }
      }
  }
        `

  return {
    query: getgetUserBadgeQuery,

    variables: {
      where: {
        user_id: {
          equals: "",
        },
      },
    },
  }
}

export { getBadge }
