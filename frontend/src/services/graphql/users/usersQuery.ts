const getUser = () => {
  const getgetUserQuery = `
    query getUsers{
      getUsers {
          user_id
          user_invite_id
          profile_pic
          total_score
          type
          name
          username
          email
          conuntry_code
          banned
          verified
          quick_start_status
          profile_description
          github_link
          twitter_link
          facebook_link
          createdAt
          updatedAt
      }
  }
        `
  return {
    query: getgetUserQuery,
    variables: {
      where: {
        user_id: {
          equals: ""
        }
      }
    },
  }
}

export { getUser }
