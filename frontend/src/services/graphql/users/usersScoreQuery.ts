const getUserScore = () => {

  const getgetUserScoreQuery = `
      query groupByUserScores{
        groupByUserScores {
          name
          totalScore
          numChallenge
        }
    }
          `;

  return {
    query: getgetUserScoreQuery,
    variables: {
      where: {
      },
    },
  };
};

export { getUserScore };
