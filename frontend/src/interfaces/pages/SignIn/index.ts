export interface SignInInterface {
    username: string,
    password: string,
}

export interface SignInUsernameInterface {
    username: string,
}

export interface SignInPasswordInterface {
    password: string,
}