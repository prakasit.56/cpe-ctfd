export interface CreateTeamInterface {
    team_profile_pic: string,
    name: string,
    country_code: string,
    github_link: string,
    twitter_link: string,
    facebook_link: string,
}

export interface TeamInterface {
    team_profile_pic: string,
}
