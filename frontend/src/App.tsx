import { StrictMode } from 'react'
import * as Urql from 'urql'

import { AppRoute } from 'adapters'
import { Header, Footer } from 'components/Modules'
import urqlClient from 'services/graphql/urqlClient'
import { useAuth } from 'store/recoil'
import { useRecoilState } from 'recoil'

import  'pages/component.css'

const App = () => {
  const [token, setToken] = useRecoilState(useAuth.selector.selectorToken)

  return (
    <Urql.Provider value={urqlClient(token, setToken)}>
      <StrictMode>
        <Header />
          <AppRoute />
        <Footer />
      </StrictMode>
    </Urql.Provider>
  )
}

export default App
