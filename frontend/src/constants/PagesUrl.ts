// Public
const HOME_PAGE_PUBLIC = "/";
const SIGN_UP_PAGE = "/sign-up";
const SIGN_IN_PAGE = "/sign-in";

// Private
const HOME_PAGE_PRIVATE = "/home";
const TUTORIAL_PAGE = "/tutorial";
// const LEADERBOARD_PAGE = "/score-leaderboard";
const MAIN_ENENT_PAGE = "/main-events";
const PROFILE_SETTING = "/profile-setting";
const CREATE_TEAM = "/create-team";
const CREATE_CHALLENGE = "/create-challenge";
const MAIN_CHALLENGE = "/main-challenge";
const CREATE_FLAG_CHALLENGE = "/create-flag-challenge";
const CREATE_CHALLENGE_SHORT_ANSWER = "/create-challenge-short-answer";
const CREATE_CHALLENGE_MULTIPLE_CHOICE = "/create-challenge-multiple-choice";
const SCORE_LEADERBOARD = "/score-leaderboard";
const TIME_SCORE_LEADERBOARD = "/time-score-leaderboard";
const TEAM_SCORE_LEADERBOARD = "/team-score-leaderboard";
const BADGE = "/badge";
const COURSE = "/course";
const TEAM_PROFILE = "/team-profile";
const TEAM_SETTING = "/team-setting";
const PROFILE = "/profile";
const EVENT = "/main-event";
const EVENT_PASS_PAGE = "/event/pass";
const CHALLENGE_PASS_PAGE = "/challenge/pass";
const FLAG_CHALLENGE = "/flag-challenge";
const CHALLENGE_MULTIPLE_CHOICE = "/challenge-multiple-choice";
const CHALLENGE_SHORT_ANSWER = "/challenge-short-answer";
const TUTORIAL_CHALLENGE = "/tutorial-challenge";
const TUTORIAL_QUICKSTART = "/tutorial-challenge";
const HOME_QUICKSTART = "/tutorial-challenge";
const LEADERBOARD_QUICKSTART = "/tutorial-challenge";
const CHALLENGE_QUICKSTART = "/tutorial-challenge";
const PROFILE_QUICKSTART = "/tutorial-challenge";
const TEAM_QUICKSTART = "/tutorial-challenge";


// ETC
const NOT_FOUND = "*";
const TEST = "/test";

export {
  HOME_PAGE_PUBLIC,
  SIGN_UP_PAGE,
  SIGN_IN_PAGE,

  HOME_PAGE_PRIVATE,
  TUTORIAL_PAGE,
  // LEADERBOARD_PAGE,
  MAIN_ENENT_PAGE,
  PROFILE_SETTING,
  CREATE_TEAM,
  CREATE_CHALLENGE,
  MAIN_CHALLENGE,
  CREATE_FLAG_CHALLENGE,
  CREATE_CHALLENGE_SHORT_ANSWER,
  CREATE_CHALLENGE_MULTIPLE_CHOICE,
  SCORE_LEADERBOARD,
  TIME_SCORE_LEADERBOARD,
  TEAM_SCORE_LEADERBOARD,
  BADGE,
  COURSE,
  TEAM_PROFILE,
  TEAM_SETTING,
  PROFILE,
  EVENT,
  EVENT_PASS_PAGE,
  CHALLENGE_PASS_PAGE,
  FLAG_CHALLENGE,
  CHALLENGE_MULTIPLE_CHOICE,
  CHALLENGE_SHORT_ANSWER,
  TUTORIAL_CHALLENGE,
  TUTORIAL_QUICKSTART,
  HOME_QUICKSTART,
  LEADERBOARD_QUICKSTART,
  CHALLENGE_QUICKSTART,
  PROFILE_QUICKSTART,
  TEAM_QUICKSTART,
  
  NOT_FOUND,
  TEST,
};