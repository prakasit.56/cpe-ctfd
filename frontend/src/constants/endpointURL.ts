import { SERVER_URL } from "constants/getEnv";

// GraphQL URL
const GRAPHQL_URL = `https://${SERVER_URL}/api/data/graphql`;

// Storage URL
//const STORAGE_URL = `https://${getEnv.SERVER_URL}/storage`;

const STORAGE_URL = 'https://'

export { GRAPHQL_URL, STORAGE_URL };
