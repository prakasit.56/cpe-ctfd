import * as Yup from 'yup'
import { FIELD_REQUIRED } from 'constants/validateSchemaForm/errorMessage'

export const SignupValidationSchema = () =>
  Yup.object().shape({
    username: Yup.string().required(
        FIELD_REQUIRED('Username is require'),
    ),
    email: Yup.string().required(
        FIELD_REQUIRED('Email is require'),
    ),
    password: Yup.string().required(
        FIELD_REQUIRED('Password is require'),
    ),
  })
