import * as Yup from 'yup'
import { FIELD_REQUIRED } from 'constants/validateSchemaForm/errorMessage'

export const CreateTeamValidationSchema = () =>
  Yup.object().shape({
    name: Yup.string().required(
        FIELD_REQUIRED('Team Name is require'),
    ),
    country_code: Yup.string(),
    github_link: Yup.string(),
    twitter_link: Yup.string(),
    facebook_link: Yup.string(),
  })
