import { PagesURL } from 'constants/index'

/**
 * `Refresh()` is a function that reloads the page
 */
const Refresh = () => window.location.reload();

/**
 * It takes a string and changes the URL to that string
 * @param {string} page - The page to go to.
 */
const Goto = (page:string) => window.location.href = page;

const NewTabTo = (page: string) => window.open(page, "_blank") || window.location.replace(page);

/**
 * Redirect to Dashboard Page
 */
const Redirect = () => Goto(PagesURL.MAIN_ENENT_PAGE);

export {
    Refresh,
    Redirect,
    Goto,
    NewTabTo
}