import { FC } from "react";
import { BrowserRouter, Routes, Route} from "react-router-dom";

import { Public, Private, Test } from 'pages'
import { PagesURL } from 'constants/index'
import { NotFound, PrivateRoute, PublicRoute } from 'adapters'

const AppRoute: FC = () => {
  return (
  <BrowserRouter>
    <Routes>
      <Route path={PagesURL.HOME_PAGE_PUBLIC} element={<PublicRoute RouteComponent={<Public.HomePage />}/>}/>
      <Route path={PagesURL.SIGN_IN_PAGE} element={<PublicRoute RouteComponent={<Public.SignInPage />}/>}/>
      <Route path={PagesURL.SIGN_UP_PAGE} element={<PublicRoute RouteComponent={<Public.SignUpPage />}/>}/>
      
      <Route path={PagesURL.MAIN_ENENT_PAGE} element={<PrivateRoute RouteComponent={<Public.MainEventPage />}/>}/>
      <Route path={PagesURL.TUTORIAL_PAGE} element={<PrivateRoute RouteComponent={<Private.TutorialPage />}/>}/>
      {/* <Route path={PagesURL.LEADERBOARD_PAGE} element={<PrivateRoute RouteComponent={<Private.TutorialPage />}/>}/> */}

      <Route path={PagesURL.PROFILE_SETTING} element={<PrivateRoute RouteComponent={<Private.ProfileSetting />}/>}/>
      <Route path={PagesURL.CREATE_TEAM } element={<PrivateRoute RouteComponent={<Private.CreateTeam />}/>}/>
      <Route path={PagesURL.CREATE_CHALLENGE} element={<PrivateRoute RouteComponent={<Private.CreateChallenge />}/>}/>
      <Route path={PagesURL.MAIN_CHALLENGE} element={<PrivateRoute RouteComponent={<Private.MainChallenge />}/>}/>
      <Route path={PagesURL.CREATE_FLAG_CHALLENGE} element={<PrivateRoute RouteComponent={<Private.CreateFlagChallenge />}/>}/>
      <Route path={PagesURL.CREATE_CHALLENGE_SHORT_ANSWER} element={<PrivateRoute RouteComponent={<Private.CreateShortAnswer />}/>}/>
      <Route path={PagesURL.CREATE_CHALLENGE_MULTIPLE_CHOICE} element={<PrivateRoute RouteComponent={<Private.CreateMultipleChoice />}/>}/>
      <Route path={PagesURL.SCORE_LEADERBOARD} element={<PrivateRoute RouteComponent={<Private.ScoreLeaderboard />}/>}/>
      <Route path={PagesURL.TIME_SCORE_LEADERBOARD} element={<PrivateRoute RouteComponent={<Private.TimeScoreLeaderboard />}/>}/>
      <Route path={PagesURL.TEAM_SCORE_LEADERBOARD} element={<PrivateRoute RouteComponent={<Private.TeamScoreLeaderboard />}/>}/>
      <Route path={PagesURL.BADGE} element={<PrivateRoute RouteComponent={<Private.Badge />}/>}/>
      <Route path={PagesURL.COURSE} element={<PrivateRoute RouteComponent={<Private.Course />}/>}/>
      <Route path={PagesURL.TEAM_PROFILE} element={<PrivateRoute RouteComponent={<Private.TeamProfile />}/>}/>
      <Route path={PagesURL.TEAM_SETTING} element={<PrivateRoute RouteComponent={<Private.TeamSetting />}/>}/>
      <Route path={PagesURL.PROFILE} element={<PrivateRoute RouteComponent={<Private.Profile />}/>}/>
      <Route path={PagesURL.EVENT} element={<PrivateRoute RouteComponent={<Private.MainEvent />}/>}/>
      <Route path={PagesURL.EVENT_PASS_PAGE} element={<PrivateRoute RouteComponent={<Private.PassEvent />}/>}/>
      <Route path={PagesURL.CHALLENGE_PASS_PAGE} element={<PrivateRoute RouteComponent={<Private.PassChallenge />}/>}/>
      <Route path={PagesURL.FLAG_CHALLENGE} element={<PrivateRoute RouteComponent={<Private.FlagChallenge />}/>}/>
      <Route path={PagesURL.CHALLENGE_MULTIPLE_CHOICE} element={<PrivateRoute RouteComponent={<Private.ChallengeMultipleChoice />}/>}/>
      <Route path={PagesURL.CHALLENGE_SHORT_ANSWER} element={<PrivateRoute RouteComponent={<Private.ChallengeShortAnswer />}/>}/>
      <Route path={PagesURL.TUTORIAL_CHALLENGE } element={<PrivateRoute RouteComponent={<Private.TutorialChallenge />}/>}/>
      <Route path={PagesURL.TUTORIAL_QUICKSTART} element={<PrivateRoute RouteComponent={<Private.TutorialQuickStart />}/>}/>
      <Route path={PagesURL.HOME_QUICKSTART} element={<PrivateRoute RouteComponent={<Private.HomeQuickStart />}/>}/>
      <Route path={PagesURL.LEADERBOARD_QUICKSTART} element={<PrivateRoute RouteComponent={<Private.LeaderboardQuickStart />}/>}/>
      <Route path={PagesURL.CHALLENGE_QUICKSTART} element={<PrivateRoute RouteComponent={<Private.ChallengeQuickStart />}/>}/>
      <Route path={PagesURL.PROFILE_QUICKSTART} element={<PrivateRoute RouteComponent={<Private.ProfileQuickStart />}/>}/>
      <Route path={PagesURL.TEAM_QUICKSTART} element={<PrivateRoute RouteComponent={<Private.TeamQuickStart />}/>}/>


      <Route path={PagesURL.NOT_FOUND} element={<NotFound path={PagesURL.HOME_PAGE_PUBLIC}/>}/>
      <Route path={PagesURL.TEST} element={<PrivateRoute RouteComponent={<Test.Test />}/>}/>
    </Routes>
  </BrowserRouter>
  );
};

export default AppRoute;