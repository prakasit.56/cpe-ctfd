import { FC } from "react";

import { LayoutContent } from 'components/Layouts'

interface ITestRouteProps {
  RouteComponent: JSX.Element
}

const TestRoute: FC<ITestRouteProps> = ({ RouteComponent }) => {

  return <LayoutContent>{RouteComponent}</LayoutContent>
};

export default TestRoute;
